SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 5/12/2020
-- Description:	Stored procedure that returns all duplicate CRD numbers in Salesforce Live environment
-- =============================================
--EXEC SalesForce_Reporting.dbo.[ContactDupeCRDs]
CREATE PROCEDURE [dbo].[ContactDupeCRDs]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	with cte_SFContact as (
select *
From openquery(SALESFORCEDEVART, 'Select id, CRD__c, Email, FirstName, LastName, Broker_Dealer__c, RecordTypeId, Dually_Registered__c from Contact')
), cte_SFBroker_Dealer as (
select *
From openquery(SALESFORCEDEVART, 'Select id, Firm_CRD__c from Broker_Dealer__c ')
), cte_SFRecordType as (
select distinct *
From openquery(SALESFORCEDEVART, 'select Id, Name from RecordType')
)


--drop table #temp
select c.*,
rt.Name as RecordTypeName,
bd.Firm_CRD__c
into #temp
from cte_SFBroker_Dealer bd
join cte_SFContact c on c.Broker_Dealer__c = bd.id
join cte_SFRecordType rt on rt.id = c.RecordTypeId


select 'https://na57.salesforce.com/'+id as SFURL,
t.CRD__c,
totalDuplicateCRDs
from (
--First, look for CRD # duplicates
select CRD__c,
count(*) as totalDuplicateCRDs
From #temp
where CRD__c is not null
group by CRD__c
having count(*) >1) as dupes
join #temp t on t.CRD__c = dupes.CRD__c


END
GO
