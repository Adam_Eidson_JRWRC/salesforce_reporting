SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 7/28/2021
-- Description:	Determine LinkedEntityId to use for Content Version pdf document
-- =============================================
--EXEC SF_FULLCOPY.GetCultureIndexLinkedEntityId @email='rsherrod14@gmail.com'
CREATE PROCEDURE [SF_FULLCOPY].[GetCultureIndexLinkedEntityId]
	@email varchar(150)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select Id
From SalesForce_Reporting.SF_FULLCOPY.vEmployee 
where email__c = @email
and status__c != 'Inactive'
union
select Id
From SalesForce_Reporting.SF_FULLCOPY.vJob_Candidate
where email__c = @email
and status__c != 'Hired'
END
GO
GRANT EXECUTE ON  [SF_FULLCOPY].[GetCultureIndexLinkedEntityId] TO [JRW\etl_user]
GO
