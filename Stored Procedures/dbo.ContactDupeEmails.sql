SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 5/12/2020
-- Description:	Stored procedure that returns all duplicate Emails in Salesforce Live environment
-- =============================================
--EXEC Salesforce_Reporting.[dbo].[ContactDupeEmails]

CREATE PROCEDURE [dbo].[ContactDupeEmails]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	with cte_SFContact as (
select *
From openquery(SALESFORCEDEVART, 'Select id, CreatedDate, Status__c, LeadSource, OtherPhone, Phone, BD_Firm__c, RIA_Firm__c, MobilePhone, HomePhone, Title, Roles__c, Discovery_Firm_Website__c, Discovery_LinkedIn_Page__c, Discovery_Personal_Web_Page__c, Discovery_Home_Phone__c, Discovery_HQ_Phone__c, Discovery_Branch_Phone__c,  Discovery_Personal_Email__c ,Discovery_Secondary_Email__c, Discovery_Email__c, CRD__c, Email, FirstName, MiddleName, LastName, Suffix,  Broker_Dealer__c, RecordTypeId, Dually_Registered__c, FINRA_Status__c, Prior_Firm_Name__c, Date_Became_Rep__c,	Licenses_Held__c, Other_Email_Addresses__c  from Contact')
), cte_SFBroker_Dealer as (
select *
From openquery(SALESFORCEDEVART, 'Select id, Name, Firm_CRD__c, RecordTypeId, FINRA_Status__c from Broker_Dealer__c ')
), cte_SFRecordType as (
select distinct *
From openquery(SALESFORCEDEVART, 'select Id, Name from RecordType')
)


--drop table #temp
select c.*,
rt.Name as RecordTypeName,
bd.Firm_CRD__c as [Broker-Dealer/RIA Firm CRD],
bd.Name as [Broker-Dealer/RIA Name],
bd.FINRA_Status__c as [Broker-Dealer/RIA FINRA Status],
bdrt.Name as [Broker-Dealer/RIA Record Type],
bd_firm.Name as [BD Firm Name],
ria_firm.Name as [RIA Firm Name]
into #temp
from cte_SFBroker_Dealer bd
join cte_SFContact c on c.Broker_Dealer__c = bd.id
join cte_SFRecordType rt on rt.id = c.RecordTypeId
join cte_SFRecordType bdrt on bdrt.id = bd.recordTypeId
left join cte_SFBroker_Dealer bd_firm on bd_firm.id = c.BD_Firm__c
left join cte_SFBroker_Dealer ria_firm on ria_firm.id = c.RIA_Firm__c

select 'https://na57.salesforce.com/'+id as SFURL,
t.Email,
TotalDuplicateEmails,
t.CRD__c,
t.CreatedDate, 
t.FirstName,
t.MiddleName,
t.LastName,
t.Suffix,
t.LeadSource,
t.Discovery_Firm_Website__c,
Discovery_LinkedIn_Page__c,
Discovery_Personal_Web_Page__c,
Discovery_Home_Phone__c,
Discovery_HQ_Phone__c,
Discovery_Branch_Phone__c,
Discovery_Personal_Email__c ,
Discovery_Secondary_Email__c, 
Discovery_Email__c,
t.[Broker-Dealer/RIA FINRA Status],
t.[Broker-Dealer/RIA Firm CRD],
t.Roles__c,
t.Title,
t.OtherPhone, 
t.Phone,
t.MobilePhone, 
t.HomePhone,
t.Prior_Firm_Name__c, 
t.Date_Became_Rep__c,	
t.Licenses_Held__c, 
t.Other_Email_Addresses__c,
t.[RIA Firm Name],
t.[BD Firm Name],
t.[Broker-Dealer/RIA Record Type],
t.Dually_Registered__c,
t.RecordTypeName,
t.FINRA_Status__c
from (
--next, look for Email duplicates
select Email, 
count(*) as TotalDuplicateEmails
from #temp
where Email is not null
group by Email
having count(*) >1) as dupes
join #temp t on t.Email = dupes.Email
END
GO
