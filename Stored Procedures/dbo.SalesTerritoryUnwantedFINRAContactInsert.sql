SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 8/25/2020
-- Description:	Stored procedure that returns SF Contact records where the WSer has signaled that they do not 
-- want new contacts inserted into their territory that were created by the FINRA/SEC ETL operation.
-- =============================================
CREATE PROCEDURE [dbo].[SalesTerritoryUnwantedFINRAContactInsert]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   
DECLARE @DateTimeBound as datetime

Select @DateTimeBound = DATEADD(Hour,-1,GETDATE())



;with cte_Contact as (
Select *
From openquery(SALESFORCEDEVART,'select id, CreatedById, Territory__c, CreatedDate, LeadSource from Contact')
), cte_SFUser as (
select * 
from openquery(SALESFORCEDEVART,'select id, Name from User')

)
select distinct cc.id as ContactId,
cc.Territory__c as Original_Sales_Territory__c,
sfUnknownTerritory.User_Id__c as Territory_Override__c,
'pending' as Status__c
from cte_Contact cc
join cte_SFUser sfCreatedBy on sfCreatedBy.id = cc.CreatedById
join openquery(SALESFORCEDEVART,'select User_Id__c from ER_Sales_Territory_Exception_Setting__mdt') sfTerritoryException on sfTerritoryException.User_Id__c = cc.territory__c
cross join openquery(SALESFORCEDEVART, 'select User_Id__c from ER_Sales_Territory_Setting__mdt where Sales_Region__c = ''Unassigned''') sfUnknownTerritory
where sfCreatedBy.Name = 'ETL API'
and CreatedDate > @DateTimeBound
and LeadSource = 'FINRA/SEC ETL'

END
GO
