SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 7/16/2021
-- Description:	Get distinct list of Contact Email as well as parsed Other Email Addresses.
-- =============================================
CREATE PROCEDURE [dbo].[ContactEmailAndOtherEmailAddresses]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select *
into #Temp_Contact
From OPENQUERY(SALESFORCEDEVART,'select Id, Email, Other_email_Addresses__c from Contact where Email != '''' and isPersonAccount = 0')

insert into #Temp_Contact
(Id, 
Email,
Other_Email_Addresses__c
)
select tc0.Id, value, tc0.Other_Email_Addresses__c
From #Temp_Contact tc0

cross apply STRING_SPLIT(REPLACE(REPLACE(REPLACE(Other_email_Addresses__c,' ',CHAR(10)),';',CHAR(10)), ',',CHAR(10)), CHAR(10)) tc1
left join #Temp_Contact tc2 on tc2.email = tc1.value
where value != ''
and tc2.id is null

Select Email
From #Temp_Contact

END
GO
GRANT EXECUTE ON  [dbo].[ContactEmailAndOtherEmailAddresses] TO [JRW\etl_user]
GO
