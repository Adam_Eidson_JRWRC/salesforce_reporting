SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 3/31/2021
-- Description:	Returns broker agent(s) to insert into SF for listings that already exist in SF.
-- =============================================
CREATE PROCEDURE [SF_FULLCOPY].[BrokerAgentToInsert]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
DECLARE @PropertySourceRecordType varchar(50)

Select @PropertySourceRecordType = (
select distinct id
From SalesForce_Reporting.SF_FULLCOPY.vPropertySourceRecordType
)


SELECT distinct ba.FirstName,
ba.LastName,
isnull(ba.email,'') as Email,
@PropertySourceRecordType as RecordTypeId,
'Broker / Agent' as Occupation__c,
'Aggregator' as LeadSource,
'Active' as Status__c,
'0014000000sKjZG' as AccountId
--,case when cab.SellerBrokerage is not null 
--then cab.SellerBrokerage
--else sb.name
--end as sellerBrokerage
--ba.phone
  FROM [AcquisitionCandidateAggregator].[dbo].ListingBrokerOfRecord lbor
  join [AcquisitionCandidateAggregator].[dbo].listing l on l.id = lbor.ListingId
  join AcquisitionCandidateAggregator.dbo.SellerBrokerage sb on sb.id = l.SellerBrokerageId
  join [AcquisitionCandidateAggregator].[dbo].[BrokerAgent] ba on lbor.name = ba.FirstName+' '+ba.LastName
  left join [AcquisitionCandidateAggregator].[dbo].[BrokerAgentLicense] bal on ba.id = bal.BrokerAgentId
  join SalesForce_Reporting.SF_FULLCOPY.vPropertyWithListingURL SF_property on SF_Property.Listing_Website_Id__c = l.ListingWebsiteId 
  left join SalesForce_Reporting.SF_FULLCOPY.vBrokerAgent vba on vba.FirstName+' '+vba.LastName = ba.FirstName+' '+ba.LastName
															--and vba.email = ba.email
  --left join AcquisitionCandidateAggregator.STAGE.CREXiAssetBroker cab on cab.ListingId = l.ListingWebsiteId
  
  where vba.id is null
END
GO
GRANT EXECUTE ON  [SF_FULLCOPY].[BrokerAgentToInsert] TO [JRW\etl_user]
GO
