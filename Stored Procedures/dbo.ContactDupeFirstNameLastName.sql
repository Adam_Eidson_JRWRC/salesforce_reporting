SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 5/12/2020
-- Description:	Stored procedure that returns all duplicate FirstName and LastName records in Salesforce Live environment
-- =============================================
--EXEC SalesForce_Reporting.[dbo].[ContactDupeFirstNameLastName]
CREATE PROCEDURE [dbo].[ContactDupeFirstNameLastName]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	with cte_SFContact as (
select *
From openquery(SALESFORCEDEVART, 'Select id, CreatedDate, Status__c, LeadSource, OtherPhone, Phone, BD_Firm__c, RIA_Firm__c, MobilePhone, HomePhone, Title, Roles__c, Discovery_Firm_Website__c, Discovery_LinkedIn_Page__c, Discovery_Personal_Web_Page__c, Discovery_Home_Phone__c, Discovery_HQ_Phone__c, Discovery_Branch_Phone__c,  Discovery_Personal_Email__c ,Discovery_Secondary_Email__c, Discovery_Email__c, CRD__c, Email, FirstName, MiddleName, LastName, Suffix,  Broker_Dealer__c, RecordTypeId, Dually_Registered__c, FINRA_Status__c, Prior_Firm_Name__c, Date_Became_Rep__c,	Licenses_Held__c, Other_Email_Addresses__c  from Contact')
), cte_SFBroker_Dealer as (
select *
From openquery(SALESFORCEDEVART, 'Select id, Firm_CRD__c , Name, FINRA_Status__c, RecordTypeId from Broker_Dealer__c ')
), cte_SFRecordType as (
select distinct *
From openquery(SALESFORCEDEVART, 'select Id, Name from RecordType')
)


--drop table #temp
select c.*,
rt.Name as RecordTypeName,
bd.Firm_CRD__c as [Broker-Dealer/RIA Firm CRD],
bd.Name as [Broker-Dealer/RIA Name],
bd.FINRA_Status__c as [Broker-Dealer/RIA FINRA Status],
bdrt.Name as [Broker-Dealer/RIA Record Type],
bd_firm.Name as [BD Firm Name],
ria_firm.Name as [RIA Firm Name]
into #temp
from cte_SFContact c
left join cte_SFBroker_Dealer bd on c.Broker_Dealer__c = bd.id
join cte_SFRecordType rt on rt.id = c.RecordTypeId
left join cte_SFRecordType bdrt on bdrt.id = bd.recordTypeId
left join cte_SFBroker_Dealer bd_firm on bd_firm.id = c.BD_Firm__c
left join cte_SFBroker_Dealer ria_firm on ria_firm.id = c.RIA_Firm__c
where rt.Name in ('Dually Registered',
'Broker-Dealer',
'RIA')



select 'https://na57.salesforce.com/'+id as SFURL,
t.Email,
totalDuplicateNames,
t.CRD__c,
t.CreatedDate, 
t.FirstName,
ISNULL(t.MiddleName, '') as MiddleName,
t.LastName,
ISNULL(t.Suffix, '') as Suffix,
t.LeadSource,
t.Discovery_Firm_Website__c,
Discovery_LinkedIn_Page__c,
Discovery_Personal_Web_Page__c,
Discovery_Home_Phone__c,
Discovery_HQ_Phone__c,
Discovery_Branch_Phone__c,
Discovery_Personal_Email__c ,
Discovery_Secondary_Email__c, 
Discovery_Email__c,
t.[Broker-Dealer/RIA FINRA Status],
t.[Broker-Dealer/RIA Firm CRD],
t.Roles__c,
t.Title,
t.OtherPhone, 
t.Phone,
t.MobilePhone, 
t.HomePhone,
t.Prior_Firm_Name__c, 
t.Date_Became_Rep__c,	
t.Licenses_Held__c, 
t.Other_Email_Addresses__c,
t.[RIA Firm Name],
t.[BD Firm Name],
t.[Broker-Dealer/RIA Record Type],
t.Dually_Registered__c,
t.RecordTypeName,
t.FINRA_Status__c,
dupes.CRD__c
from (

--next, look for duplicate first and last names
select LastName, 
FirstName,
CRD__c,
count(*) as totalDuplicateNames
from #temp
where LastName is not null
and firstName is not null
group by LastName, firstname,CRD__c
having count(*) >1) as dupes
join #temp t on t.LastName = dupes.LastName 
			and t.FirstName = dupes.FirstName
			and ISNULL(t.CRD__c ,'')=ISNULL(dupes.CRD__c,'')
			order by t.firstName, t.lastname
END
GO
