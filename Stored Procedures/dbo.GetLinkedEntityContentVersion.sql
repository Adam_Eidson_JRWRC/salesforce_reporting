SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Adam Eidson
-- Create date: 7/29/2021
-- Description:	Takes the Linked Entity Id parameter and fetches all documents in SF that are associated with this Linked Entity Id.
-- =============================================
--EXEC SalesForce_Reporting.dbo.GetLinkedEntityContentVersion @LinkedEntityId='a304C000000DG83QAG'
CREATE PROCEDURE [dbo].[GetLinkedEntityContentVersion]
	-- Add the parameters for the stored procedure here
	@LinkedEntityId varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--create dynamic sql statement
	declare @sql varchar(max) = 'select *
From openquery(SALESFORCEDEVART, ''Select cv.Title, cv.Id, cdl.LinkedEntityId, cv.ContentDocumentId from ContentVersion cv
join ContentDocumentLink cdl on cdl.ContentDocumentId = cv.ContentDocumentId
where cdl.LinkedEntityId = '''''+@LinkedEntityId+''''''')'

--execute dynamic sql statement
	execute( @sql)
END
GO
GRANT EXECUTE ON  [dbo].[GetLinkedEntityContentVersion] TO [JRW\etl_user]
GO
