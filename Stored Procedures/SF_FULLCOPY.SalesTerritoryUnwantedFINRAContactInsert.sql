SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 8/25/2020
-- Description:	Stored procedure that returns SF Contact records where the WSer has signaled that they do not 
-- want new contacts inserted into their territory that were created by the FINRA/SEC ETL.
-- =============================================
CREATE PROCEDURE [SF_FULLCOPY].[SalesTerritoryUnwantedFINRAContactInsert]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   
DECLARE @DateTimeBound as datetime

Select @DateTimeBound = DATEADD(Hour,-1,GETDATE())



;with cte_Contact as (
Select *
From openquery(SALESFORCEDEVARTFULLCOPY,'select id, CreatedById, Territory__c, CreatedDate, LeadSource, CRD__c, Status__c from Contact')
), cte_SFUser as (
select * 
from openquery(SALESFORCEDEVARTFULLCOPY,'select id, Name from User')

), cte_KeepCRD as (
SELECT distinct [CRDNumber]      
  FROM [RegulatoryAgency].[dbo].[vIndividualLicense]
  where REPLACE(LicenseName, 'Series ','') in ('7','22','7TO','22TO','65')
  ), cte_ValidLicense as (
  select vilf.*
  from [RegulatoryAgency].[dbo].[vIndividualLicense_Flattened] vilf
  left join cte_KeepCRD ckc on ckc.CRDNumber = vilf.CRDNumber
  where ckc.CRDNumber is not null
  )
select distinct cc.id as Id,
cc.Territory__c as Original_Sales_Territory__c,
sfUnknownTerritory.User_Id__c as Territory_Override__c,
case when cvl.CRDNumber is not null 
then 'Pending'
else cc.Status__c
end as Status__c
from cte_Contact cc
join cte_SFUser sfCreatedBy on sfCreatedBy.id = cc.CreatedById
join openquery(SALESFORCEDEVARTFULLCOPY,'select User_Id__c from ER_Sales_Territory_Exception_Setting__mdt') sfTerritoryException on sfTerritoryException.User_Id__c = cc.territory__c
left join cte_ValidLicense cvl on cvl.CRDNumber = cc.CRD__c
cross join openquery(SALESFORCEDEVARTFULLCOPY, 'select User_Id__c from ER_Sales_Territory_Setting__mdt where Sales_Region__c = ''Unassigned''') sfUnknownTerritory
where sfCreatedBy.Name = 'ETL API'
and CreatedDate > @DateTimeBound
and LeadSource = 'FINRA/SEC ETL'



END
GO
GRANT EXECUTE ON  [SF_FULLCOPY].[SalesTerritoryUnwantedFINRAContactInsert] TO [JRW\etl_user]
GO
