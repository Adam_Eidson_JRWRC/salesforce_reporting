SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 5/12/2020
-- Description:	Stored procedure that returns all duplicate LastName records in Salesforce Live environment
-- =============================================
--EXEC SalesForce_Reporting.[SF_FULLCOPY].[ContactDupeLastName]
CREATE PROCEDURE [SF_FULLCOPY].[ContactDupeLastName]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	with cte_SFContact as (
select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'Select id, CRD__c, Email, FirstName, LastName, Broker_Dealer__c, RecordTypeId, Dually_Registered__c from Contact')
), cte_SFBroker_Dealer as (
select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'Select id, Firm_CRD__c from Broker_Dealer__c ')
), cte_SFRecordType as (
select distinct *
From openquery(SALESFORCEDEVARTFULLCOPY, 'select Id, Name from RecordType')
)


--drop table #temp
select c.*,
rt.Name as RecordTypeName,
bd.Firm_CRD__c
into #temp
from cte_SFBroker_Dealer bd
join cte_SFContact c on c.Broker_Dealer__c = bd.id
join cte_SFRecordType rt on rt.id = c.RecordTypeId

--next, look for duplicate last names
;with cte_lastNameDupes as (
select LastName, 
count(*) as TotalDupes
from #temp
where LastName is not null
group by LastName
having count(*) >1
), cte_fullData1 as(
select t.* 
from #temp t
join cte_lastNameDupes clnd on clnd.lastname = t.lastName
), cte_fullData2 as(
select t.* 
from #temp t
join cte_lastNameDupes clnd on clnd.lastname = t.lastName

)
select distinct 'https://na57.salesforce.com/'+cfd1.id as Record1_URL, 
'https://na57.salesforce.com/'+cfd2.id as Record2URL,
cfd1.lastname as Record1_LastName,
cfd2.lastname as Record2_LastName,
cfd1.firstname as Record1_FirstName,
cfd2.firstname as Record1_FirstName
From cte_fullData1 cfd1
join cte_fullData2 cfd2 on cfd1.id <> cfd2.id
and cfd1.lastName = cfd2.lastName
--and substring(cfd1.firstName,1,1) = substring(cfd2.firstname,1,1)
order by cfd1.lastName

END
GO
