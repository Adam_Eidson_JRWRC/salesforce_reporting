SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 7/10/2020
-- Description:	Stored procedure that returns missing BD/RIA Association records where a Contact 
-- has a Broker-Dealer/RIA value, but no BD/RIA Association record
-- =============================================
--exec SalesForce_Reporting.SF_FULLCOPY.GetMissingBD_RIA_Association
CREATE PROCEDURE [SF_FULLCOPY].[GetMissingBD_RIA_Association]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
--select distinct SFContact.CRD__c
--into #tempQuestionableCRD
--From SalesForce_Reporting.dbo.[vContactsDistinct] SFContact
--left join RegulatoryAgency.dbo.individual i on i.CRDNumber = SFContact.CRD__c
--where SFContact.LastName != i.LastName

declare @FinraSEC_RecordType varchar(50)
  --get FINRA/SEC record Type Id
  select @FinraSEC_RecordType = id
  from SalesForce_Reporting.SF_FULLCOPY.vBDRIAAssociationRecordType 
  where name = 'Other'


;
with cte_missing as (
--get all broker-dealer/ria field values 
select sfContact.CRD__c,
SFContact.Broker_Dealer__c,
SFContact.id
From SalesForce_Reporting.SF_FULLCOPY.[vContactsDistinct] SFContact
left join SalesForce_Reporting.SF_FULLCOPY.vContactRecordType RT on RT.id = SFContact.RecordTypeId
--get all bd/ria associations
left join SalesForce_Reporting.SF_FULLCOPY.vBD_RIA_Association SFBDRIA on SFBDRIA.Broker_Dealer__c = SFContact.Broker_Dealer__c
																	and SFBDRIA.Contact__c = SFContact.id
where SFBDRIA.id is null
and SFContact.CRD__c is null
and SFContact.Broker_Dealer__c is not null
--and FINRA_Status__c = 'Active'
)
--check if Contact CRD has an active registration
select distinct cm.Broker_Dealer__c, cm.id as Contact__c,
'true' as Active__c,
@FinraSEC_RecordType as RecordTypeId
From cte_missing cm -- on cm.CRD__c = vif.individualCRDNumber
--and endDate is null
--left join #tempQuestionableCRD cqc on cqc.CRD__c = vif.individualCRDNumber
--where cqc.CRD__c is null

END
GO
GRANT EXECUTE ON  [SF_FULLCOPY].[GetMissingBD_RIA_Association] TO [ETL_User]
GO
