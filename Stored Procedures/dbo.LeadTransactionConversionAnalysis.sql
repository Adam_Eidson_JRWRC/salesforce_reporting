SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 8/18/2020
-- Description:	Data prep for serving Lead data to analysis mechanism for Decision Tree and transaction likelihood modeling
-- =============================================
--exec Salesforce_reporting.dbo.LeadTransactionConversionAnalysis
CREATE PROCEDURE [dbo].[LeadTransactionConversionAnalysis]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	with cte_prep as (
  select SFLeads.*
	  ,[Age1]
      ,[In_A_1031_Transaction__c]
      ,[X1031_Interest__pc]
      ,[X1031_Producer_2__pc]
      ,[X1031_45th_Day__c]
      ,[CreatedDate]
      ,[CreatedTill60thDay]
	  ,Lead_Created_Date__c
	  ,[LeadCreatedTill60thDay]
,case when cl.ClientId is not null
then 1
else 0
end
as validTrade

from vSFAccountInvestmentPrefsExperience SFLeads
left join SalesForce_Reporting.dbo.vClientsWithNonCancelledTrades cl on cl.ClientId = SFLeads.id
), cte_prep2 as( 

select [id]
      ,[Accredited__c]
      ,[Individual_Investments_Allow_Higher_Risk__c]
      ,[Liquidity_Needs__c]
      ,[Allows_for_Speculation__c]
      ,[Time_Horizon__c]
      ,[Investment_Objective__c]
      ,cast(Inv_Category_Preferences__c as varchar(200)) as Inv_Category_Preferences__c
	  ,cast(Asset_Class_Preferences__c as varchar(200)) as Asset_Class_Preferences__c
      ,[Stocks__c]
      ,[Bonds__c]
      ,[Options__c]
      ,[Mutual_Funds__c]
      ,[DPP__c]
      ,[REITs__c]
      ,[Annuities__c]
      ,[Investment_Real_Estate__c]
      ,[Alternatives__c]
	  ,[Age1]
      ,[In_A_1031_Transaction__c]
      ,[X1031_Interest__pc]
      ,[X1031_Producer_2__pc]
      ,[X1031_45th_Day__c]
      ,[CreatedDate]
      ,[CreatedTill60thDay]
	  ,Lead_Created_Date__c
	  ,[LeadCreatedTill60thDay]
      ,[value]
	  ,validTrade

from cte_prep
)
select [id]
      ,[Accredited__c]
      ,[Individual_Investments_Allow_Higher_Risk__c]
      ,[Liquidity_Needs__c]
      ,[Allows_for_Speculation__c]
      ,[Time_Horizon__c]
      ,[Investment_Objective__c]
      ,[Inv_Category_Preferences__c]
      ,[Asset_Class_Preferences__c]
      ,[Stocks__c]
      ,[Bonds__c]
      ,[Options__c]
      ,[Mutual_Funds__c]
      ,[DPP__c]
      ,[REITs__c]
      ,[Annuities__c]
      ,[Investment_Real_Estate__c]
      ,[Alternatives__c]
	  ,[Age1]
      ,[In_A_1031_Transaction__c]
      ,[X1031_Interest__pc]
      ,[X1031_Producer_2__pc]
      ,[X1031_45th_Day__c]
      ,[CreatedDate]
      ,[CreatedTill60thDay]
	  ,Lead_Created_Date__c
	  ,[LeadCreatedTill60thDay]
      ,[value]
	  ,max(validTrade) as validTrade
	  into #temp
	  from cte_prep2
group by [id]
      ,[Accredited__c]
      ,[Individual_Investments_Allow_Higher_Risk__c]
      ,[Liquidity_Needs__c]
      ,[Allows_for_Speculation__c]
      ,[Time_Horizon__c]
      ,[Investment_Objective__c]
      ,[Inv_Category_Preferences__c]
      ,[Asset_Class_Preferences__c]
      ,[Stocks__c]
      ,[Bonds__c]
      ,[Options__c]
      ,[Mutual_Funds__c]
      ,[DPP__c]
      ,[REITs__c]
      ,[Annuities__c]
      ,[Investment_Real_Estate__c]
      ,[Alternatives__c]
	  ,[Age1]
      ,[In_A_1031_Transaction__c]
      ,[X1031_Interest__pc]
      ,[X1031_Producer_2__pc]
      ,[X1031_45th_Day__c]
      ,[CreatedDate]
      ,[CreatedTill60thDay]
	  ,Lead_Created_Date__c
	  ,[LeadCreatedTill60thDay]
      ,[value]
	  
DECLARE @columns as varchar(MAX)

select @columns = coalesce(@columns + ', ','') + QUOTENAME(value) 
from (
select distinct value from #temp
where value!= ''
) as b
order by b.value


DECLARE @sql varchar(MAX)
Select @sql = '
drop table SalesForce_reporting.dbo.LeadConversionAnalysis

Select *
into SalesForce_reporting.dbo.LeadConversionAnalysis
From 
(
select id, 
Accredited__c,
Individual_Investments_Allow_Higher_Risk__c,
Liquidity_Needs__c,
Allows_for_Speculation__c, 
Time_Horizon__c,
Investment_Objective__c , 
cast(Inv_Category_Preferences__c as varchar(200)) as Inv_Category_Preferences__c,   
cast(Asset_Class_Preferences__c as varchar(200)) as Asset_Class_Preferences__c,    
Stocks__c, 
Bonds__c, 
Options__c, 
Mutual_Funds__c, 
DPP__c, 
REITs__c, 
Annuities__c, 
	  [Age1]
      ,[In_A_1031_Transaction__c]
      ,[X1031_Interest__pc]
      ,[X1031_Producer_2__pc]
      ,[X1031_45th_Day__c]
      ,[CreatedDate]
      ,[CreatedTill60thDay]
	  ,Lead_Created_Date__c
	  ,[LeadCreatedTill60thDay]
	  ,Investment_Real_Estate__c, 
 Alternatives__c,
 value,
 validTrade
From #temp
) as t
pivot 
( min(value) 
		for value in ('+@columns+')
) as s
'
EXEC (@sql)
END

GO
