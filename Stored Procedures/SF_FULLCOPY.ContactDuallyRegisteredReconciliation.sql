SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 5/12/2020
-- Description:	Stored procedure that returns all records that have conflicting Dually Registered and Record Type values in Salesforce Live environment
-- =============================================
--EXEC SalesForce_Reporting.[SF_FULLCOPY].ContactDuallyRegisteredReconciliation
CREATE PROCEDURE [SF_FULLCOPY].[ContactDuallyRegisteredReconciliation]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	with cte_SFContact as (
select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'Select id, CRD__c, Email, FirstName, LastName, Broker_Dealer__c, RecordTypeId, Dually_Registered__c from Contact')
), cte_SFBroker_Dealer as (
select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'Select id, Firm_CRD__c from Broker_Dealer__c ')
), cte_SFRecordType as (
select distinct *
From openquery(SALESFORCEDEVARTFULLCOPY, 'select Id, Name from RecordType')
)


--drop table #temp
select c.*,
rt.Name as RecordTypeName,
bd.Firm_CRD__c
into #temp
from cte_SFBroker_Dealer bd
join cte_SFContact c on c.Broker_Dealer__c = bd.id
join cte_SFRecordType rt on rt.id = c.RecordTypeId


--Next, check record Type and Dually Registered
select *
From #temp
where Dually_Registered__c = 'Yes'
and RecordTypeName != 'Dually Registered'
union
select *
From #temp
where Dually_Registered__c = 'No'
and RecordTypeName = 'Dually Registered'
END
GO
