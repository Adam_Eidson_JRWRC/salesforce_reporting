SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 7/28/2021
-- Description:	Checks if the Culture Index PDF exists in SF based on Survey Id
-- =============================================
--EXEC SF_FULLCOPY.CultureIndexCheckIfPDFExists @SurveyId= '7552414'
CREATE PROCEDURE [SF_FULLCOPY].[CultureIndexCheckIfPDFExists]
	-- Add the parameters for the stored procedure here
	@SurveyId varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	select @SurveyId='CultureIndex-'+@SurveyId
   
declare @LinkedEntityId varchar(50)

--Select @SurveyId = 'Culture Index: 7915776'

;with cte_JobAssessment as (
select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'select Candidate__c, Employee__c, RecordTypeId, Submission_Id__c from Job_Assessment__c')

), cte_RecordType as (

select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'Select Id from RecordType where Name = ''Cultural Index''')
)

select @LinkedEntityId = case when ja.Candidate__c is not null
then ja.Candidate__c
when ja.Employee__c is not null
then ja.Employee__c
else ''
end 
From [Assessment].[CultureIndex].[vSurveyResult] sr
join cte_JobAssessment ja on ja.Submission_Id__c = sr.SurveyId
--left join cte_JobCandidate c on sr.email = c.email__c
join cte_RecordType rt on rt.Id = ja.RecordTypeId 
where ja.Submission_Id__c = @SurveyId--Email__c = @Email

select @LinkedEntityId= case when @LinkedEntityId is null
then ''
end

declare @sql varchar(max) = 'select *
From openquery(SALESFORCEDEVARTFULLCOPY, ''Select cv.Title, cv.Id, cdl.LinkedEntityId, cv.ContentDocumentId from ContentVersion cv
join ContentDocumentLink cdl on cdl.ContentDocumentId = cv.ContentDocumentId
where cdl.LinkedEntityId = '''''+@LinkedEntityId+''''''')'

execute( @sql)


END
GO
GRANT EXECUTE ON  [SF_FULLCOPY].[CultureIndexCheckIfPDFExists] TO [JRW\etl_user]
GO
