SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Adam Eidson
-- Create date: 7/29/2021
-- Description:	Determine LinkedEntityId to use for Content Version pdf document
-- =============================================
--EXEC dbo.GetCultureIndexLinkedEntityId @email='rsherrod14@gmail.com'
CREATE PROCEDURE [dbo].[GetCultureIndexLinkedEntityId]
	@email varchar(150)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select Id
From SalesForce_Reporting.dbo.vEmployee 
where email__c = @email
and status__c != 'Inactive'
union
select Id
From SalesForce_Reporting.dbo.vJob_Candidate
where email__c = @email
and status__c != 'Hired'
END
GO
GRANT EXECUTE ON  [dbo].[GetCultureIndexLinkedEntityId] TO [JRW\etl_user]
GO
