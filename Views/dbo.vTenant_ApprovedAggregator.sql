SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vTenant_ApprovedAggregator] as 
select *
From openquery(SALESFORCEDEVART, 'Select Id, Name, Aliases__c, Approved_Aggregator__c From Tenant__c where Approved_Aggregator__c = 1')
GO
