SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vFirmRecordType] as 
select *
From openquery(SalesforceDevart, 'select rt.Id, rt.Name  from Broker_Dealer__c c
join RecordType rt on rt.id = c.RecordTypeId
group by rt.Id, rt.name')
GO
