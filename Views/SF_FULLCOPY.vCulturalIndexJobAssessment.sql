SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [SF_FULLCOPY].[vCulturalIndexJobAssessment] as
Select distinct ja.Id, 
ja.Submission_Id__c,  
ja.Candidate__c,
ja.Employee__c
from SF_FULLCOPY.vJob_Assessment ja
join SF_FULLCOPY.vCulturalIndexRecordType rt on rt.Id = ja.RecordTypeId 
left join SF_FULLCOPY.vJob_Candidate c on c.Id = ja.Candidate__c
left join SF_FULLCOPY.vEmployee e on e.Id = ja.Employee__c

GO
