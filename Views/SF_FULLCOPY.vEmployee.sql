SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [SF_FULLCOPY].[vEmployee] as 

select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'select Email__c, Id, Name, Status__c from Employee__c')
GO
