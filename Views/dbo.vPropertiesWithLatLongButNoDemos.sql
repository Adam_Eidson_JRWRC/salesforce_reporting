SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vPropertiesWithLatLongButNoDemos]
as
select vp.Id as BrokerWebsiteId,
'Salesforce' as BrokerWebsiteSource, Address_Geocode__latitude__s as Latitude, Address_Geocode__longitude__s as Longitude
,SUBSTRING(CONVERT(varchar(50),GETDATE(),126),0,LEN(CONVERT(varchar(50),GETDATE(),126))-3) Demo_Last_Updated__c
From SalesForce_Reporting.dbo.vProperty vp
left join SalesForce_Reporting.dbo.vDemographic vd on vp.id = vd.Property__c
where vd.id is null
and vp.Address_Geocode__latitude__s is not null
GO
