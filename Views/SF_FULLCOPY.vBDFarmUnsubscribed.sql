SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [SF_FULLCOPY].[vBDFarmUnsubscribed] as 
SELECT  distinct rf.ToEmailAddress
  FROM [MailApp].[STAGE].[CampaignEngagement-BDFarm] rf
  left join SalesForce_Reporting.[SF_FULLCOPY].vContactsDistinct vcd on vcd.Email = rf.ToEmailAddress
  where (Unsubscribed != 0)
  and vcd.id is null
GO
