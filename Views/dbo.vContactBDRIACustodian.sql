SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [dbo].[vContactBDRIACustodian] as

select  ContactId,
FirmId,
Custodial_Platform__c
From openquery(SALESFORCEDEVART, 'select c.id as ContactId ,vbd.id as FirmId, vbd.Custodial_Platform__c from Contact c
join BD_RIA_Association__c bra on bra.Contact__c = c.id
join Broker_Dealer__c vbd on  vbd.id = bra.Broker_Dealer__c
where vbd.Custodial_Platform__c is not null')
GO
