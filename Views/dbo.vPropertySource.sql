SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vPropertySource] as 
select *
from openquery(SALESFORCEDEVART, 'Select id, Name from Property_Source__c')
GO
