SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [dbo].[vPropertyToFetchLatLong] as 
SELECT  distinct  [Id] as ListingId
,[Street__c]+ ', '+isnull([City__c],'')+', '+isnull([State__c],'')+', '+isnull([Zip__c],'') as Address
  FROM [SalesForce_Reporting].[dbo].[vProperty]
  where Address_Geocode__latitude__s is null
  and Street__c is not null
  and city__c is not null
  and state__c is not null
  --and createdDate >= dateAdd(day, -15,getdate())
GO
