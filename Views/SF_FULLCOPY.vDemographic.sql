SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE view [SF_FULLCOPY].[vDemographic] as

select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'Select Id, Property__c from Demographic__c')

GO
