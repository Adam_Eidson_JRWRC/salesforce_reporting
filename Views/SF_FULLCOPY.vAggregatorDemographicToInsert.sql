SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE View [SF_FULLCOPY].[vAggregatorDemographicToInsert] as
select distinct 
SF_Property.id as Property__c
,Listing.[1MileRadiusPopulation] as X1_Mile_Radius_Population__c
      ,Listing.[MedianHouseholdIncome] as X1_Mile_Median_Household_Income__c
      ,Listing.[1MileRadiusVacantHousing] as X1_Mile_Vacant_Housing_Units__c
      ,Listing.[1MileRadiusOwnerOccupiedHousing] as X1_Mile_Owner_Occupied_Housing_Units__c
      ,Listing.[1MileRadiusRenterOccupiedHousing] as X1_Mile_Renter_Occupied_Housing_Units__c
      ,Listing.[1MileRadiusAvgHHIncome] as X1_Mile_Radius_Avg_HH_Income__c
      ,Listing.[3MileRadiusPopulation] as X3_Mile_Radius_Population__c
      ,Listing.[3MileRadiusVacantHousing] as X3_Mile_Vacant_Housing_Units__c
      ,Listing.[3MileRadiusOwnerOccupiedHousing] as X3_Mile_Owner_Occupied_Housing_Units__c
      ,Listing.[3MileRadiusRenterOccupiedHousing] as X3_Mile_Renter_Occupied_Housing_Units__c
      ,Listing.[3MileRadiusAvgHHIncome] as X3_Mile_Radius_Avg_HH_Income__c
      ,Listing.[5MileRadiusPopulation] as X5_Mile_Radius_Population__c
      ,Listing.[5MileRadiusVacantHousing] as X5_Mile_Vacant_Housing_Units__c
      ,Listing.[5MileRadiusOwnerOccupiedHousing] as X5_Mile_Owner_Occupied_Housing_Units__c
      ,Listing.[5MileRadiusRenterOccupiedHousing] as X5_Mile_Renter_Occupied_Housing_Units__c
      ,Listing.[CrimeRateIndex] as Crime_Rate_Index__c
      ,Listing.[Mile5PctOfIncomeForMortgage] as X5_Mile_Pct_of_Income_for_Mortgage__c
      ,Listing.[Mile5GrowthRateOwnerOccHUs] as X5_Mile_Growth_Rate_Owner_Occ_HUs__c
      ,Listing.[Mile5CivPop16PlusLaborForce] as X5_Mile_Civ_Pop_16_Labor_Force__c
      ,Listing.[Mile5EmployedCivilianPop16Plus] as X5_Mile_Employed_Civilian_Pop_16__c
      ,Listing.[Mile5UnemployedPopulation16Plus] as X5_Mile_Unemployed_Population_16__c
      ,Listing.[Mile5EmployedCivilianPop16To24] as X5_Mile_Employed_Civilian_Pop_16_24__c
      ,Listing.[Mile5UnemployedPop16To24] as X5_Mile_Unemployed_Pop_16_24__c
      ,Listing.[Mile5UnempRatePop16To24] as X5_Mile_Unemp_Rate_Pop_16_24__c
      ,Listing.[Mile5EmployedCivilianPop25To54] as X5_Mile_Employed_Civilian_Pop_25_54__c
      ,Listing.[Mile5UnemployedPop25To54] as X5_Mile_Unemployed_Pop_25_54__c
      ,Listing.[Mile5EmployedCivilianPop55To64] as X5_Mile_Employed_Civilian_Pop_55_64__c
      ,Listing.[Mile5UnemployedPop55To64] as X5_Mile_Unemployed_Pop_55_64__c
      ,Listing.[Mile5EmployedCivilianPop65Plus] as X5_Mile_Employed_Civilian_Pop_65__c
      ,Listing.[Mile5UnemployedPop65Plus] as X5_Mile_Unemployed_Pop_65__c
      ,Listing.[Mile5Emp16PlusByIndustryBase] as X5_Mile_Emp_16_by_Industry_Base__c
      ,Listing.[Mile5IndustryAgriculture] as X5_Mile_Industry_Agriculture__c
      ,Listing.[Mile5IndustryConstruction] as X5_Mile_Industry_Construction__c
      ,Listing.[Mile5IndustryManufacturing] as X5_Mile_Industry_Manufacturing__c
      ,Listing.[Mile5IndustryWholesaleTrade] as X5_Mile_Industry_Wholesale_Trade__c
      ,Listing.[Mile5IndustryRetailTrade] as X5_Mile_Industry_Retail_Trade__c
      ,Listing.[Mile5IndustryTransportation] as X5_Mile_Industry_Transportation__c
      ,Listing.[Mile5IndustryUtilities] as X5_Mile_Industry_Utilities__c
      ,Listing.[Mile5IndustryInformation] as X5_Mile_Industry_Information__c
      ,Listing.[Mile5IndustryFinanceAndInsurance] as X5_Mile_Industry_Finance_Insurance__c
      ,Listing.[Mile5IndustryRealEstate] as X5_Mile_Industry_Real_Estate__c
      ,Listing.[Mile5IndustryProfessionalAndTechSvcs] as X5_Mile_Industry_Professional_Tech_Svcs__c
      ,Listing.[Mile5IndustryAdminAndWasteMgmt] as X5_Mile_Industry_Admin_Waste_Mgmt__c
      ,Listing.[Mile5IndustryHealthCare] as X5_Mile_Industry_Health_Care__c
      ,Listing.[Mile5IndustryAccommodationAndFoodSvcs] as X5_Mile_Industry_Accommodation_Food_Svcs__c
      ,Listing.[Mile5IndustryOtherServices] as X5_Mile_Industry_Other_Services__c
      ,Listing.[Mile5IndustryPublicAdministration] as X5_Mile_Industry_Public_Administration__c
      ,Listing.[Mile3PctOfIncomeForMortgage] as X3_Mile_Pct_of_Income_for_Mortgage__c
      ,Listing.[Mile3GrowthRateOwnerOccHUs] as X3_Mile_Growth_Rate_Owner_Occ_HUs__c
      ,Listing.[Mile3CivPop16PlusLaborForce] as X3_Mile_Civ_Pop_16_Labor_Force__c
      ,Listing.[Mile3EmployedCivilianPop16Plus] as X3_Mile_Employed_Civilian_Pop_16__c
      ,Listing.[Mile3UnemployedPopulation16Plus] as X3_Mile_Unemployed_Population_16__c
      ,Listing.[Mile3EmployedCivilianPop16To24] as X3_Mile_Employed_Civilian_Pop_16_24__c
      ,Listing.[Mile3UnemployedPop16To24] as X3_Mile_Unemployed_Pop_16_24__c
      ,Listing.[Mile3UnempRatePop16To24] as X3_Mile_Unemp_Rate_Pop_16_24__c
      ,Listing.[Mile3EmployedCivilianPop25To54] as X3_Mile_Employed_Civilian_Pop_25_54__c
      ,Listing.[Mile3UnemployedPop25To54] as X3_Mile_Unemployed_Pop_25_54__c
      ,Listing.[Mile3EmployedCivilianPop55To64] as X3_Mile_Employed_Civilian_Pop_55_64__c
      ,Listing.[Mile3UnemployedPop55To64] as X3_Mile_Unemployed_Pop_55_64__c
      ,Listing.[Mile3EmployedCivilianPop65Plus] as X3_Mile_Employed_Civilian_Pop_65__c
      ,Listing.[Mile3UnemployedPop65Plus] as X3_Mile_Unemployed_Pop_65__c
      ,Listing.[Mile3Emp16PlusByIndustryBase] as X3_Mile_Emp_16_by_Industry_Base__c
      ,Listing.[Mile3IndustryAgriculture] as X3_Mile_Industry_Agriculture__c
      ,Listing.[Mile3IndustryConstruction] as X3_Mile_Industry_Construction__c
      ,Listing.[Mile3IndustryManufacturing] as X3_Mile_Industry_Manufacturing__c
      ,Listing.[Mile3IndustryWholesaleTrade] as X3_Mile_Industry_Wholesale_Trade__c
      ,Listing.[Mile3IndustryRetailTrade] as X3_Mile_Industry_Retail_Trade__c
      ,Listing.[Mile3IndustryTransportation] as X3_Mile_Industry_Transportation__c
      ,Listing.[Mile3IndustryUtilities] as X3_Mile_Industry_Utilities__c
      ,Listing.[Mile3IndustryInformation] as X3_Mile_Industry_Information__c
      ,Listing.[Mile3IndustryFinanceAndInsurance] as X3_Mile_Industry_Finance_Insurance__c
      ,Listing.[Mile3IndustryRealEstate] as X3_Mile_Industry_Real_Estate__c
      ,Listing.[Mile3IndustryProfessionalAndTechSvcs] as X3_Mile_Industry_Professional_Tech_Svcs__c
      ,Listing.[Mile3IndustryAdminAndWasteMgmt] as X3_Mile_Industry_Admin_Waste_Mgmt__c
      ,Listing.[Mile3IndustryHealthCare] as X3_Mile_Industry_Health_Care__c
      ,Listing.[Mile3IndustryAccommodationAndFoodSvcs] as X3_Mile_Industry_Accommodation_Food_Svcs__c
      ,Listing.[Mile3IndustryOtherServices] as X3_Mile_Industry_Other_Services__c
      ,Listing.[Mile3IndustryPublicAdministration] as X3_Mile_Industry_Public_Administration__c
      ,Listing.[Mile1PctOfIncomeForMortgage] as X1_Mile_Pct_of_Income_for_Mortgage__c
      ,Listing.[Mile1GrowthRateOwnerOccHUs] as X1_Mile_Growth_Rate_Owner_Occ_HUs__c
      ,Listing.[Mile1CivPop16PlusLaborForce] as X1_Mile_Civ_Pop_16_Labor_Force__c
      ,Listing.[Mile1EmployedCivilianPop16Plus] as X1_Mile_Employed_Civilian_Pop_16__c
      ,Listing.[Mile1UnemployedPopulation16Plus] as X1_Mile_Unemployed_Population_16__c
      ,Listing.[Mile1EmployedCivilianPop16To24] as X1_Mile_Employed_Civilian_Pop_16_24__c
      ,Listing.[Mile1UnemployedPop16To24] as X1_Mile_Unemployed_Pop_16_24__c
      ,Listing.[Mile1UnempRatePop16To24] as X1_Mile_Unemp_Rate_Pop_16_24__c
      ,Listing.[Mile1EmployedCivilianPop25To54] as X1_Mile_Employed_Civilian_Pop_25_54__c
      ,Listing.[Mile1UnemployedPop25To54] as X1_Mile_Unemployed_Pop_25_54__c
      ,Listing.[Mile1EmployedCivilianPop55To64] as X1_Mile_Employed_Civilian_Pop_55_64__c
      ,Listing.[Mile1UnemployedPop55To64] as X1_Mile_Unemployed_Pop_55_64__c
      ,Listing.[Mile1EmployedCivilianPop65Plus] as X1_Mile_Employed_Civilian_Pop_65__c
      ,Listing.[Mile1UnemployedPop65Plus] as X1_Mile_Unemployed_Pop_65__c
      ,Listing.[Mile1Emp16PlusByIndustryBase] as X1_Mile_Emp_16_by_Industry_Base__c
      ,Listing.[Mile1IndustryAgriculture] as X1_Mile_Industry_Agriculture__c
      ,Listing.[Mile1IndustryConstruction] as X1_Mile_Industry_Construction__c
      ,Listing.[Mile1IndustryManufacturing] as X1_Mile_Industry_Manufacturing__c
      ,Listing.[Mile1IndustryWholesaleTrade] as X1_Mile_Industry_Wholesale_Trade__c
      ,Listing.[Mile1IndustryRetailTrade] as X1_Mile_Industry_Retail_Trade__c
      ,Listing.[Mile1IndustryTransportation] as X1_Mile_Industry_Transportation__c
      ,Listing.[Mile1IndustryUtilities] as X1_Mile_Industry_Utilities__c
      ,Listing.[Mile1IndustryInformation] as X1_Mile_Industry_Information__c
      ,Listing.[Mile1IndustryFinanceAndInsurance] as X1_Mile_Industry_Finance_Insurance__c
      ,Listing.[Mile1IndustryRealEstate] as X1_Mile_Industry_Real_Estate__c
      ,Listing.[Mile1IndustryProfessionalAndTechSvcs] as X1_Mile_Industry_Professional_Tech_Svcs__c
      ,Listing.[Mile1IndustryAdminAndWasteMgmt] as X1_Mile_Industry_Admin_Waste_Mgmt__c
      ,Listing.[Mile1IndustryHealthCare] as X1_Mile_Industry_Health_Care__c
      ,Listing.[Mile1IndustryAccommodationAndFoodSvcs] as X1_Mile_Industry_Accommodation_Food_Svcs__c
      ,Listing.[Mile1IndustryOtherServices] as X1_Mile_Industry_Other_Services__c
      ,Listing.[Mile1IndustryPublicAdministration] as X1_Mile_Industry_Public_Administration__c
	  ,SUBSTRING(CONVERT(varchar(50),GETDATE(),126),0,LEN(CONVERT(varchar(50),GETDATE(),126))-3) Demo_Last_Updated__c-- yyyy-MM-ddTHH:mm:ss
FROM [AcquisitionCandidateAggregator].[dbo].[vBrokerWebsiteDataApprovedTenant] Listing
  left join SalesForce_Reporting.SF_FULLCOPY.vBrokerAgent vba on vba.firstName+' '+vba.lastName = Listing.[BrokerOfRecordName]
  --left join SalesForce_Reporting.dbo.vTenant SF_tenant on SF_tenant.Name = Listing.Tenant
  join SalesForce_Reporting.SF_FULLCOPY.vPropertyWithListingURL SF_property on SF_Property.Listing_Website_Id__c = Listing.ListingWebsiteId 
																			--and SF_Property.Name = SUBSTRING(Listing.[Name],0,80)--and SF_property.Listing_URL__c = Listing.ListingURL
				and isNull(convert(varchar(15),Listing.ContractPrice),0) = SF_Property.Contract_Price__c
  --join SalesForce_Reporting.dbo.vPropertySource vps on cast(isnull(vps.Name ,'')as varchar(50)) = cast(isnull(Listing.sellerBrokerageName , '')as varchar(50))
  left join SalesForce_Reporting.SF_FULLCOPY.vdemographic SF_Demo on SF_Demo.Property__c = SF_property.Id
  where Listing.[5MileRadiusPopulation] is not null
  and Listing.[Mile1IndustryPublicAdministration] is not null
  and SF_Demo.id Is null
GO
