SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



  CREATE VIEW [dbo].[vPropertyToDelete] as
  
  with cte_dupes as (
  select distinct Listing_Website_Id__c, 
  Name,
  Contract_Price__c
  from SalesForce_Reporting.dbo.vPropertyWithListingURL
  group by Listing_Website_Id__c,
  Name,
  Contract_Price__c
  having count(*) >1
  )

  select distinct sf.id
  from SalesForce_Reporting.dbo.vPropertyWithListingURL sf
  join cte_dupes as d on ISNULL(d.Listing_Website_Id__c,'') = ISNULL(sf.Listing_Website_Id__c,'')
  and ISNULL(d.Name,'') = ISNULL(sf.Name,'')
  and ISNULL(d.Contract_Price__c,'') = ISNULL(sf.Contract_Price__c,'')
  where sf.stage__c = 'Listing-Active'
  and d.Listing_Website_Id__c not in ('7',
'6',
'5',
'461',
'4',
'13',
'13',
'1')
GO
