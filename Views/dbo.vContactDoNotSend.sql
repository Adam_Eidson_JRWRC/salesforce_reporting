SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE View [dbo].[vContactDoNotSend] as 
with cte_SFContact as (
select *
from openquery(SALESFORCEDEVART,' Select Id, Email,Broker_Dealer__c, Roles__c from Contact where email is not null') 

)
Select distinct sfContact.Email,
'DNS: '+Subscription__c as DNSTag
From openquery(SALESFORCEDEVART, 'Select Id ,Contact__c, Broker_Dealer__c, Level__c,Do_Not_Send__c,  Subscription__c from Email_Preference__c where Do_Not_Send__c = 1') SFEmailPref

join cte_SFContact SFContact on SFContact.Broker_Dealer__c = SFEmailPref.Broker_Dealer__c
where Level__c = 'Campaign Group'
and Roles__c not like '%Due Diligence%'

union 
Select SFContact.Email,
'DNS: '+Subscription__c as DNSTag
From openquery(SALESFORCEDEVART, 'Select Id ,Contact__c, Broker_Dealer__c, Level__c, Subscription__c from Email_Preference__c where Do_Not_Send__c = 1') SFEmailPref
join cte_SFContact SFContact on SFContact.id = SFEmailPref.Contact__c
where Level__c = 'Campaign Group'
GO
