SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vFirmsToSyncWithFINRASEC] as 
with cte_FirmsWSellingAgreement as (
select * 
from openquery(SALESFORCEDEVART, 'Select id, Firm_CRD__c, Signed_Selling_Agreement__c, Key_Account__c from Broker_Dealer__c where Firm_CRD__c != null ')
),cte_Firms as (
select * 
from openquery(SALESFORCEDEVART, 'Select id, Firm_CRD__c, All_Signed_Selling_Agreements__c from Broker_Dealer__c where Firm_CRD__c != null ')
), cte_Investors as (
select *
From openquery(SALESFORCEDEVART, 'select id, Broker_Dealer_ER__c from Account where Broker_Dealer_ER__c != null and stage__c = ''Closed and Funded''')
), cte_CRDRefreshedRecently as (
select distinct CRD
From RegulatoryAgency.SF_FULLCOPY.CRDRefreshTracking 
where LastQuery > DATEADD(day, -1, GETDATE())
and ObjectType = 'Firm'
)

select cast(Firm_CRD__c as varchar(50)) as Firm_CRD__c
From cte_FirmsWSellingAgreement csa
left join cte_CRDRefreshedRecently crt on crt.CRD = csa.Firm_CRD__c
where (key_Account__c = 1
or signed_Selling_Agreement__c = 1)
and Firm_CRD__c not in ('133763',
'10299',
'6363',
'7072',
'8158',
'13686',
'7870',
'35747',
'6413')
and crt.CRD is Null
union
select cast(cf.Firm_CRD__c as varchar(50)) as Firm_CRD__c
from cte_Firms cf
join cte_Investors ci on ci.Broker_Dealer_ER__c = cf.id
left join cte_CRDRefreshedRecently crt on crt.CRD = cf.Firm_CRD__c
where  Firm_CRD__c not in ('133763',
'10299',
'6363',
'7072',
'8158',
'13686',
'7870',
'35747',
'6413')
and crt.CRD is Null
GO
