SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














CREATE VIEW [SF_FULLCOPY].[vProperty] as 
Select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'Select Id, Name,Address_Geocode__latitude__s, Address_Geocode__longitude__s,Property_Origin__c, Purchase_NOI__c, Listing_URL__c, LOI__c, Stage__c, Purchase_Price__c, tCurrent_NOI__c,X5_Mile_Radius_Population__c, crime_rate_index__c, Listing_Website_Id__c, Street__c, City__c, State__c,  Zip__c, createdDate from Property__c')
GO
