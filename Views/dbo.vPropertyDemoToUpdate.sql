SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE view [dbo].[vPropertyDemoToUpdate] as 
select distinct p.Id,
CONVERT(varchar(50),[1MileRadiusPopulation])  as  X1_Mile_Radius_Population__c
      ,CONVERT(varchar(50),[MedianHouseholdIncome]) as Median_Household_Income__c
      ,CONVERT(varchar(50),[1MileRadiusVacantHousing]) as X1_Mile_Radius_Vacant_Housing__c
      ,CONVERT(varchar(50),[1MileRadiusOwnerOccupiedHousing]) as X1_Mile_Radius_Owner_Occupied_Housing__c
      ,CONVERT(varchar(50),[1MileRadiusRenterOccupiedHousing]) as X1_Mile_Radius_Renter_Occupied_Housing__c
      ,CONVERT(varchar(50),[1MileRadiusAvgHHIncome]) as X1_Mile_Radius_Avg_HH_Income__c
      ,CONVERT(varchar(50),[3MileRadiusPopulation]) as X3_Mile_Radius_Population__c
      ,CONVERT(varchar(50),[3MileRadiusVacantHousing]) as X3_Mile_Radius_Vacant_Housing__c
      ,CONVERT(varchar(50),[3MileRadiusOwnerOccupiedHousing]) as X3_Mile_Radius_Owner_Occupied_Housing__c
      ,CONVERT(varchar(50),[3MileRadiusRenterOccupiedHousing]) as X3_Mile_Radius_Renter_Occupied_Housing__c
      ,CONVERT(varchar(50),[3MileRadiusAvgHHIncome]) as X3_Mile_Radius_Avg_HH_Income__c
      ,CONVERT(varchar(50),[5MileRadiusPopulation]) as X5_Mile_Radius_Population__c
      ,CONVERT(varchar(50),[5MileRadiusVacantHousing]) as X5_Mile_Radius_Vacant_Housing__c
      ,CONVERT(varchar(50),[5MileRadiusOwnerOccupiedHousing]) as X5_Mile_Radius_Owner_Occupied_Housing__c
      ,CONVERT(varchar(50),[5MileRadiusRenterOccupiedHousing]) as X5_Mile_Radius_Renter_Occupied_Housing__c
	  ,CONVERT(varchar(50),[CrimeRateIndex]) as Crime_Rate_Index__c
From SalesForce_Reporting.dbo.vPropertyWithListingURL p
join AcquisitionCandidateAggregator.dbo.listing d on d.[ListingWebsiteId]=  p.Listing_Website_Id__c
where p.X5_Mile_Radius_Population__c is null
and [5MileRadiusPopulation] is not null
union

select distinct p.Id,
CONVERT(varchar(50),[Mile1TotalPopulation]) as  X1_Mile_Radius_Population__c
      ,CONVERT(varchar(50),[Mile1MedianHHIncome]) as Median_Household_Income__c
      ,CONVERT(varchar(50),[Mile1VACANTHousingUnit]) as X1_Mile_Radius_Vacant_Housing__c
      ,CONVERT(varchar(50),[Mile1OwnerOccupiedHousingUnit]) as X1_Mile_Radius_Owner_Occupied_Housing__c
      ,CONVERT(varchar(50),[Mile1RentedHousingUnit]) as X1_Mile_Radius_Renter_Occupied_Housing__c
      ,CONVERT(varchar(50),[Mile1AverageHHIncome]) as X1_Mile_Radius_Avg_HH_Income__c
      ,CONVERT(varchar(50),[Mile3TotalPopulation]) as X3_Mile_Radius_Population__c
      ,CONVERT(varchar(50),[Mile3VACANTHousingUnit]) as X3_Mile_Radius_Vacant_Housing__c
      ,CONVERT(varchar(50),[Mile3OwnerOccupiedHousingUnit]) as X3_Mile_Radius_Owner_Occupied_Housing__c
      ,CONVERT(varchar(50),[Mile3RentedHousingUnit]) as X3_Mile_Radius_Renter_Occupied_Housing__c
      ,CONVERT(varchar(50),[Mile3AverageHHIncome]) as X3_Mile_Radius_Avg_HH_Income__c
      ,CONVERT(varchar(50),[Mile5TotalPopulation]) as X5_Mile_Radius_Population__c
      ,CONVERT(varchar(50),[Mile5VACANTHousingUnit]) as X5_Mile_Radius_Vacant_Housing__c
      ,CONVERT(varchar(50),[Mile5OwnerOccupiedHousingUnit]) as X5_Mile_Radius_Owner_Occupied_Housing__c
      ,CONVERT(varchar(50),[Mile5RentedHousingUnit]) as X5_Mile_Radius_Renter_Occupied_Housing__c
	  ,CONVERT(varchar(50),[CrimeIndex]) as Crime_Rate_Index__c	  
From SalesForce_Reporting.dbo.vProperty p
join AcquisitionCandidateAggregator.CallCenter.demographic d on d.[brokerWebsiteId]=  p.id
where p.X5_Mile_Radius_Population__c is  null
and [Mile5TotalPopulation] is not null
GO
