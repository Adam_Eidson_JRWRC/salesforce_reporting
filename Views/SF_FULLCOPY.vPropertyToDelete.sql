SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

  CREATE VIEW [SF_FULLCOPY].[vPropertyToDelete] as
  
  with cte_dupes as (
  select distinct Listing_Website_Id__c
  from SalesForce_Reporting.SF_FULLCOPY.vPropertyWithListingURL
  group by Listing_Website_Id__c
  having count(*) >1
  )

  select distinct sf.id
  from SalesForce_Reporting.SF_FULLCOPY.vPropertyWithListingURL sf
  join cte_dupes as d on ISNULL(d.Listing_Website_Id__c,'') = ISNULL(sf.Listing_Website_Id__c,'')
GO
