SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [dbo].[vRIAFarmBouncedAndComplained] as 
SELECT  distinct rf.ToEmailAddress
  FROM [MailApp].[STAGE].[CampaignEngagement-RIAFarm] rf
  left join SalesForce_Reporting.dbo.vContactsDistinct vcd on vcd.Email = rf.ToEmailAddress
  where (Bounced is not null
  or Complained != 0)
  and vcd.id is null
GO
