SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE view [dbo].[vPropertyAddressToUpdate] as 

select distinct p.id, Street as Street__c
,City as City__c,
State as State__c,
Zip as Zip__c
From SalesForce_Reporting.dbo.vPropertyWithListingURL p
join AcquisitionCandidateAggregator.dbo.Listing l on  l.ListingWebsiteId=  p.Listing_Website_Id__c
and p.Contract_Price__c = l.ContractPrice
where l.city is not null
--and p.street__c is null
and p.city__c is null
GO
