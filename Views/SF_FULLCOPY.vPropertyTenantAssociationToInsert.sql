SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [SF_FULLCOPY].[vPropertyTenantAssociationToInsert] as
select SF_Listing.id as Property__c,
SF_Tenant.id as Tenant__c
from SF_FullCopy.vPropertyWithListingURL SF_Listing
join AcquisitionCandidateAggregator.dbo.vBrokerWebsiteDataApprovedTenant aca on aca.[PropertyURL] = SF_Listing.Listing_URL__c
join SF_FULLCOPY.vTenant SF_Tenant on SF_Tenant.name = aca.Tenant
left join SF_FULLCOPY.vPropertyTenantAssociation vpta on vpta.Property__c = SF_Listing.id
and vpta.Tenant__c = SF_Tenant.id
where vpta.id is null
GO
