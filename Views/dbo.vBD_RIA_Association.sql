SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE view [dbo].[vBD_RIA_Association] as
select distinct id, Broker_Dealer__c, Contact__c, active__c, RecordTypeId
from openquery(SALESFORCEDEVART, 'Select id, Broker_Dealer__c, Contact__c, active__c , RecordTypeId from BD_RIA_Association__c')
GO
