SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vClientRecordType] as 
select *
From openquery(SalesforceDevart, 'select rt.Id, rt.Name  from Account c
join RecordType rt on rt.id = c.RecordTypeId
where rt.name in (''Client'') group by rt.Id, rt.name')
GO
