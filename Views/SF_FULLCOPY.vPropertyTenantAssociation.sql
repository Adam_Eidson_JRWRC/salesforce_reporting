SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [SF_FULLCOPY].[vPropertyTenantAssociation] as 
select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'Select id, Property__c, Tenant__c from Property_Tenant_Association__c')
GO
