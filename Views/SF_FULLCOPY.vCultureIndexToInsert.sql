SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [SF_FULLCOPY].[vCultureIndexToInsert] as
with cte_CandidateAssessment as (
select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'Select jc.email__c, ja.Submission_Id__c  from Job_Assessment__c ja
join Job_Candidate__c jc on jc.Id = ja.Candidate__c
where RecordTypeId IN (Select Id from RecordType where Name = ''Cultural Index'')')
),
cte_EmployeeAssessment as (
select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'Select jc.email__c, ja.Submission_Id__c from Job_Assessment__c ja
join Employee__c jc on jc.Id = ja.Employee__c
where RecordTypeId IN (Select Id from RecordType where Name = ''Cultural Index'')')
)
, cte_existingCultureIndexAssessment as (


select ca.email__c, ca.Submission_Id__c 
from cte_CandidateAssessment ca
union
select ce.email__c, ce.Submission_Id__c
from cte_EmployeeAssessment ce
--join cte_candidate cc on cc.id = ca.Candidate__c
--join cte_RecordType rt on rt.id = ca.RecordTypeId
--where rt.Name = 'Cultural Index'

),cte_Employee as (
select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'Select jc.email__c, Id from Employee__c jc where email__c != null')
),cte_candidate as (
select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'Select jc.email__c, Id from Job_Candidate__c jc where email__c != null and Status__c != ''Hired''')
),cte_position as (
select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'Select Name, Id from Job_Position__c')
),cte_RecordType as (
select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'Select Id from RecordType where Name = ''Cultural Index''')
), cte_FinalPrep as (

select r.SurveyId as [Submission_Id__c﻿]
,r.[Trait_A]
      ,r.[Trait_B]
      ,r.[Trait_C]
      ,r.[Trait_D]
      ,r.[Trait_L]
      ,r.[Trait_I]
      ,r.[Trait_EU]
      ,r.[Trait_Line]
      ,r.[AssessmentDate]
      ,r.[JobTitle]
      ,r.[TraitPattern]
      ,r.[JobPattern]
      ,r.[Behavior_A]
      ,r.[Behavior_B]
      ,r.[Behavior_C]
      ,r.[Behavior_D]
      ,r.[Behavior_L]
      ,r.[Behavior_I]
      ,r.[Behavior_EU]
      ,r.[Behavior_Line],
	  cp.Id as Position__c,
ce.Id as Employee__c,
null as Candidate__c
From Assessment.CultureIndex.vSurveyResult r
left join cte_existingCultureIndexAssessment eca on r.email = eca.email__c and eca.Submission_Id__c = r.SurveyId
left join cte_position cp on cp.Name = r.JobTitle
join cte_Employee ce on ce.email__c = r.email
where eca.email__c is null
union 
select r.SurveyId as [Submission_Id__c﻿],
r.[Trait_A]
      ,r.[Trait_B]
      ,r.[Trait_C]
      ,r.[Trait_D]
      ,r.[Trait_L]
      ,r.[Trait_I]
      ,r.[Trait_EU]
      ,r.[Trait_Line]
      ,r.[AssessmentDate]
      ,r.[JobTitle]
      ,r.[TraitPattern]
      ,r.[JobPattern]
      ,r.[Behavior_A]
      ,r.[Behavior_B]
      ,r.[Behavior_C]
      ,r.[Behavior_D]
      ,r.[Behavior_L]
      ,r.[Behavior_I]
      ,r.[Behavior_EU]
      ,r.[Behavior_Line],
	  cp.Id as Position__c,
null as Employee__c,
ce.Id as Candidate__c
From Assessment.CultureIndex.vSurveyResult r
left join cte_existingCultureIndexAssessment eca on r.email = eca.email__c and eca.Submission_Id__c = r.SurveyId
left join cte_position cp on cp.Name = r.JobTitle
join cte_candidate ce on ce.email__c = r.email
where eca.email__c is null
)

Select [Submission_Id__c],
Position__c,
Employee__c,
Candidate__c,
[AssessmentDate] as Submission_Date__c,
Type__c,
Result__c,
crt.Id as RecordTypeId
FROM 
(
select [Submission_Id__c﻿],
cast([Trait_A] as varchar(150)) as [Candidate Trait A] 
      ,cast([Trait_B] as varchar(150)) as [Candidate Trait B] 
      ,cast([Trait_C] as varchar(150)) as [Candidate Trait C] 
      ,cast([Trait_D] as varchar(150)) as [Candidate Trait D] 
      ,cast([Trait_L] as varchar(150)) as [Candidate Trait L] 
      ,cast([Trait_I] as varchar(150)) as [Candidate Trait I] 
      ,cast([Trait_EU] as varchar(150)) as [Candidate Trait EU] 
      ,cast([Trait_Line] as varchar(150)) as [Candidate Trait Line] 
      ,cast([TraitPattern] as varchar(150)) as [Candidate Trait Pattern] 
	  ,[AssessmentDate]
      ,cast([JobPattern] as varchar(150)) as [Job Pattern] 
      ,cast([Behavior_A] as varchar(150)) as [Job Trait A] 
      ,cast([Behavior_B] as varchar(150)) as [Job Trait B] 
      ,cast([Behavior_C] as varchar(150)) as [Job Trait C] 
      ,cast([Behavior_D] as varchar(150)) as [Job Trait D] 
      ,cast([Behavior_L] as varchar(150)) as [Job Trait L] 
      ,cast([Behavior_I] as varchar(150)) as [Job Trait I]
      ,cast([Behavior_EU] as varchar(150)) as [Job Trait EU]
      ,cast([Behavior_Line] as varchar(150))  as [Job Trait Line]
,
	  Position__c,
Employee__c,
Candidate__c
From cte_FinalPrep

) as cfp
UNPIVOT
(Result__c
FOR Type__c IN (
		[Candidate Trait A]
      ,[Candidate Trait B]
      ,[Candidate Trait C]
      ,[Candidate Trait D]
      ,[Candidate Trait L]
      ,[Candidate Trait I]
      ,[Candidate Trait EU]
      ,[Candidate Trait Line]
      ,[Candidate Trait Pattern]
      ,[Job Pattern]
      ,[Job Trait A]
      ,[Job Trait B]
      ,[Job Trait C]
      ,[Job Trait D]
      ,[Job Trait L]
      ,[Job Trait I]
      ,[Job Trait EU]
      ,[Job Trait Line]
	)
) as up
cross join 
cte_RecordType as crt

GO
