SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [dbo].[vBDFarmEngagement] as 
SELECT  distinct rf.ToEmailAddress
  FROM [MailApp].[STAGE].[CampaignEngagement-BDFarm] rf
  left join SalesForce_Reporting.dbo.vContactsDistinct vcd on vcd.Email = rf.ToEmailAddress
  where opens > 0
  or clicks > 0
  and vcd.id is null
GO
