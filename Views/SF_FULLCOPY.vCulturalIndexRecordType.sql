SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create View [SF_FULLCOPY].[vCulturalIndexRecordType] as 

select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'Select Id from RecordType where Name = ''Cultural Index''')
GO
