SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [SF_FULLCOPY].[vPropertySourceRecordType] as

select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'select rt.Id, rt.Name  from Contact c
join RecordType rt on rt.id = c.RecordTypeId
where rt.name in (''Property Source'') group by rt.Id, rt.name')
GO
