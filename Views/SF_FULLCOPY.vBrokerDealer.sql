SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [SF_FULLCOPY].[vBrokerDealer] as 
Select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'Select id, Name, Firm_CRD__c From Broker_Dealer__c')
GO
