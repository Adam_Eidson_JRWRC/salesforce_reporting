SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vContactsWithInadequateLicensing] as
with cte_SF as (
select id, Licenses_Held__c
From openquery(SalesforceDEvart, 'select id, Licenses_Held__c from Contact where Licenses_Held__c != '''' and CreatedById = ''0050b000005imtW'' and status__c = ''Active''')
where Licenses_Held__c like '%63%'
or Licenses_Held__c like '%6;%'
), cte_potentialRemovals as (
select id, l.*
from cte_SF
CROSS APPLY (select VALUE from STRING_SPLIT(cast(Licenses_Held__c as varchar(50)), ';'))  l

), cte_keepContacts as (
select sf.id,
sf.Licenses_Held__c,
pr.value
from cte_SF sf
join cte_potentialRemovals pr on sf.id = pr.id
where pr.value not in ('6','63')
and pr.value  in ('7','22','7TO','22TO','65')
)
select distinct 'https://na57.salesforce.com/'+pr.id as SF_URL,
pr.id
From cte_potentialRemovals pr
left join cte_keepContacts ck on pr.id = ck.id
where ck.id is  null
GO
