SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE view [SF_FULLCOPY].[vBD_RIA_Association] as
select distinct id, Broker_Dealer__c, Contact__c, active__c
from openquery(SALESFORCEDEVARTFULLCOPY, 'Select id, Broker_Dealer__c, Contact__c, active__c from BD_RIA_Association__c')
GO
