SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vTenant] as 
select *
From openquery(SALESFORCEDEVART, 'Select Id, Name From Tenant__c')
GO
