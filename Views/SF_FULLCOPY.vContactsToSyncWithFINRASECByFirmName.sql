SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [SF_FULLCOPY].[vContactsToSyncWithFINRASECByFirmName] as 
with cte_Firms as (
select * 
from openquery(SALESFORCEDEVARTFULLCOPY, 'Select id, Firm_CRD__c, Signed_Selling_Agreement__c, Key_Account__c, Name from Broker_Dealer__c')
), cte_Contacts as (
select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'select id, CRD__c, Broker_Dealer__c from Contact where CRD__c is not Null')
)

select distinct cc.CRD__c, cf.Name as FirmName,
cf.Firm_CRD__c
From cte_Contacts cc
join cte_Firms cf on cf.id = cc.Broker_Dealer__c
GO
