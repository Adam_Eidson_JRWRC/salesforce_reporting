SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO













CREATE VIEW [SF_FULLCOPY].[vPropertyWithListingURL] as 
Select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'Select Id, Name, Purchase_NOI__c, Listing_URL__c, Stage__c, Purchase_Price__c,Contract_Price__c, tCurrent_NOI__c,X5_Mile_Radius_Population__c, crime_rate_index__c, Listing_Website_Id__c,Seller_Brokerage__c, Street__c, City__c, State__c,  Zip__c, Property_Source__c, createdDate from Property__c where Listing_URL__c is not null')
GO
