SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE view [SF_FULLCOPY].[vDemoToCreate] as

with cte_property as (
select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'Select Id, X1_Mile_Radius_Population__c
      ,Median_Household_Income__c
	  ,X1_Mile_Radius_Vacant_Housing__c
      ,X1_Mile_Radius_Owner_Occupied_Housing__c
      ,X1_Mile_Radius_Renter_Occupied_Housing__c
      ,X1_Mile_Radius_Avg_HH_Income__c
      ,X3_Mile_Radius_Population__c
      ,X3_Mile_Radius_Vacant_Housing__c
      ,X3_Mile_Radius_Owner_Occupied_Housing__c
      ,X3_Mile_Radius_Renter_Occupied_Housing__c
      ,X3_Mile_Radius_Avg_HH_Income__c
      ,X5_Mile_Radius_Population__c
      ,X5_Mile_Radius_Vacant_Housing__c
      ,X5_Mile_Radius_Owner_Occupied_Housing__c
      ,X5_Mile_Radius_Renter_Occupied_Housing__c
	  ,Crime_Rate_Index__c from Property__c')

), cte_demo as (
select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'Select Id, Property__c from Demographic__c')
)

select cp.id Property__c
, X1_Mile_Radius_Population__c
      ,Median_Household_Income__c as X5_Mile_Median_Household_Income__c
	  ,X1_Mile_Radius_Vacant_Housing__c as X1_Mile_Vacant_Housing_Units__c
      ,X1_Mile_Radius_Owner_Occupied_Housing__c as X1_Mile_Owner_Occupied_Housing_Units__c
      ,X1_Mile_Radius_Renter_Occupied_Housing__c as X1_Mile_Renter_Occupied_Housing_Units__c
      ,X1_Mile_Radius_Avg_HH_Income__c
      ,X3_Mile_Radius_Population__c
      ,X3_Mile_Radius_Vacant_Housing__c as X3_Mile_Vacant_Housing_Units__c
      ,X3_Mile_Radius_Owner_Occupied_Housing__c as X3_Mile_Owner_Occupied_Housing_Units__c
      ,X3_Mile_Radius_Renter_Occupied_Housing__c as X3_Mile_Renter_Occupied_Housing_Units__c
      ,X3_Mile_Radius_Avg_HH_Income__c
      ,X5_Mile_Radius_Population__c
      ,X5_Mile_Radius_Vacant_Housing__c as X5_Mile_Vacant_Housing_Units__c
      ,X5_Mile_Radius_Owner_Occupied_Housing__c as X5_Mile_Owner_Occupied_Housing_Units__c
      ,X5_Mile_Radius_Renter_Occupied_Housing__c as X5_Mile_Renter_Occupied_Housing_Units__c
	  ,Crime_Rate_Index__c  
	  ,SUBSTRING(CONVERT(varchar(50),GETDATE(),126),0,LEN(CONVERT(varchar(50),GETDATE(),126))-3) Demo_Last_Updated__c-- yyyy-MM-ddTHH:mm:ss
	  From cte_property cp
left join cte_demo cd on cp.id = cd.property__c
where X5_Mile_Radius_Population__c is not null
and cd.id is null
GO
