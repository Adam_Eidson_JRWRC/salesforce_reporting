SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO












CREATE VIEW [dbo].[vContactBrokerDealerActive] as 


select  email ContactEmail,
'BD: '+Name BrokerDealerName
From openquery(SALESFORCEDEVART, 'select c.Email,vbd.Name  from Contact c
join BD_RIA_Association__c bra on bra.Contact__c = c.id
join Broker_Dealer__c vbd on  vbd.id = bra.Broker_Dealer__c
where c.Email is not null
and bra.active__c = 1')


GO
