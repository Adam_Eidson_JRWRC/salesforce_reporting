SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vTrade] as 
select *
From openquery(SalesforceDevart, 'select Client_Name__c, Id, status__c from Trade__c')
GO
