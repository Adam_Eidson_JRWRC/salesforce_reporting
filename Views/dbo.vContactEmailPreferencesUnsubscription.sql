SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vContactEmailPreferencesUnsubscription] as
with cte_SFContact as (
select *
from openquery(SALESFORCEDEVART,' Select Id, Email,Broker_Dealer__c, Roles__c from Contact where email is not null') 

)
Select distinct SFContact.Email
From openquery(SALESFORCEDEVART, 'Select Id ,Contact__c, Broker_Dealer__c, Level__c, Subscription__c from Email_Preference__c where Do_Not_Send__c = 1') SFEmailPref
join cte_SFContact SFContact on SFContact.id = SFEmailPref.Contact__c
where (Level__c = 'Company' and Subscription__c ='ExchangeRight') or 
(Level__c = 'Category' and Subscription__c ='ER Industry (BD/RIA Contacts)')
GO
