SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE view [dbo].[vDemographic] as

select *
From openquery(SALESFORCEDEVART, 'Select Id, Property__c, Crime_Rate_Index__c, X5_Mile_Total_Housing_Units__c from Demographic__c')

GO
