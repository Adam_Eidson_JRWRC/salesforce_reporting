SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


Create View [dbo].[vCulturalIndexRecordType] as 

select *
From openquery(SALESFORCEDEVART, 'Select Id from RecordType where Name = ''Cultural Index''')
GO
