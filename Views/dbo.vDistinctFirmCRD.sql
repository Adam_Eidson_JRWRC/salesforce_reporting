SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE view [dbo].[vDistinctFirmCRD] as
select distinct id, Firm_CRD__c
from openquery(SALESFORCEDEVART, 'Select id, Firm_CRD__c from Broker_Dealer__c where Firm_CRD__c != ''''')
GO
