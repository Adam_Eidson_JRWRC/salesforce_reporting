SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE View [dbo].[vBrokerAgent] as 
with cte_BA as (
Select *
From openquery(SALESFORCEDEVART, 'Select ID, RecordTypeId, FirstName, LastName, email, Title from Contact')
), cte_PropertySourceRecordType as (
select *
From openquery(SalesforceDevart, 'select rt.Id, rt.Name  from Contact c
join RecordType rt on rt.id = c.RecordTypeId
where rt.name in (''Property Source'') group by rt.Id, rt.name')
)

select ba.*
From cte_PropertySourceRecordType rt
join cte_BA ba on ba.RecordTypeId = rt.Id
GO
