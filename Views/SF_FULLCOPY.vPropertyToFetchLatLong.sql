SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [SF_FULLCOPY].[vPropertyToFetchLatLong] as 
SELECT top 10 [Id] as ListingId
,[Street__c]+ ', '+isnull([City__c],'')+', '+isnull([State__c],'')+', '+isnull([Zip__c],'') as Address
  FROM [SalesForce_Reporting].[SF_FULLCOPY].[vProperty]
  where Address_Geocode__latitude__s is null
  and Street__c is not null
  and city__c is not null
  and state__c is not null
GO
