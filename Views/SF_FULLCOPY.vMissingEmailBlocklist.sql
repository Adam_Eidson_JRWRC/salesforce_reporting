SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [SF_FULLCOPY].[vMissingEmailBlocklist] as
with cte_EmailBlocklist as (
select distinct *
From openquery(SALESFORCEDEVARTFULLCOPY, 'select email__c from Email_Blacklist__c')
), cte_BulkEmailSTatsComplainedBounced as (
select distinct *
From openquery(SALESFORCEDEVARTFULLCOPY, 'select To_Address__c, Delivery_Date__c, Bounced__c, Complained__c from Task where Complained__c = 1 or Bounced__c = ''true''')
)

select distinct To_Address__c as Email__c,
case when Bounced__c is not null 
then min(convert(varchar(100), Delivery_Date__c, 126)) 
end as Bounce_Timestamp__c,
case when Bounced__c is not null 
then 'true' 
else 'false'
end as Bounce__c,
case when Complained__c =1
then min(convert(varchar(100), Delivery_Date__c, 126))
end as Complaint_Timestamp__c, 
case when Complained__c =1
then 'true'
else 'false'
end as Complaint__c
From cte_BulkEmailSTatsComplainedBounced cbe
left join cte_EmailBlocklist ceb on cbe.To_Address__c = ceb.email__c
where ceb.email__c is null
group by To_Address__c,
Bounced__C,
Complained__c
GO
