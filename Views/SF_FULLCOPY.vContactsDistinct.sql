SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










CREATE VIEW [SF_FULLCOPY].[vContactsDistinct] as 


select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'select id, CRD__c, Broker_Dealer__c, FirstName, LastName, RecordTypeId, FINRA_Status__c, Email from Contact')


GO
