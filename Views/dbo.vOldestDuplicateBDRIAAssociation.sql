SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vOldestDuplicateBDRIAAssociation] as with cte_BDRIAAssociation as (
select Broker_Dealer__c, Contact__c,
Count(*) as totaldupes
From openquery(SALESFORCEDEVART, 'select Broker_Dealer__c, Contact__c, CreatedDate, Active__c from BD_RIA_Association__c')
group by Contact__c, Broker_Dealer__c
having count(*) >1
), cte_oldestBDRIARecord as (
select sf.Broker_Dealer__c,
sf.Contact__c,
min(CreatedDate) as minCreatedDate
From openquery(SALESFORCEDEVART, 'select Broker_Dealer__c, Contact__c, CreatedDate, Active__c from BD_RIA_Association__c') sf
join cte_BDRIAAssociation cba on cba.Broker_Dealer__c = sf.Broker_Dealer__c
								and cba.Contact__c = sf.Contact__c
group by sf.Contact__c, sf.Broker_Dealer__c
)

select sf2.*
from cte_oldestBDRIARecord sf
join openquery(SALESFORCEDEVART, 'select id, Broker_Dealer__c, Contact__c, CreatedDate, Active__c from BD_RIA_Association__c where createdById = ''0050b000005imtW''') sf2 on sf.Broker_Dealer__c = sf2.Broker_Dealer__c 
																																		and sf.Contact__c = sf2.Contact__c
																																		and sf.minCreatedDate = sf2.CreatedDate
GO
