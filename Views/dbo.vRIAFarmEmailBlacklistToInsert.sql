SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [dbo].[vRIAFarmEmailBlacklistToInsert] as 
SELECT  distinct rf.ToEmailAddress as Email__c, 
case when Bounced is not null
then 1
else 0
end as Bounce__c, 
case when Bounced is not null
then convert(varchar(100), SendTime, 126)
else null
end as Bounce_Timestamp__c, 
Complained as Complaint__c, 

case when Complained != 0
then convert(varchar(100), SendTime, 126)
else null
end as Complaint_Timestamp__c, 
case when Bounced is null
then 'Source: mailchimp | Data: {''campaign_id'': '''+CampaignId+''', ''email_address'': '''+ToEmailAddress+''', ''SendTime'': '''+convert(varchar(100), SendTime, 126)+'''}' 
else 
'Source: mailchimp | Data: {''campaign_id'': '''+CampaignId+''', ''email_address'': '''+ToEmailAddress+''', ''SendTime'': '''+convert(varchar(100), SendTime, 126)+''', ''BounceType'': '''+Bounced+'''}' 
end as Description__c 
  FROM [MailApp].[STAGE].[CampaignEngagement-RIAFarm] rf
  left join openquery(SALESFORCEDEVART, 'select Email__c from Email_Blocklist__c') sfBlacklist on sfBlacklist.Email__c = rf.ToEmailAddress
  where (Bounced is not null
  or Complained != 0)
  and sfBlacklist.Email__c is null
GO
