SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









CREATE VIEW [dbo].[vPropertyTenantAssociationToInsert] as


Select * 
From OPENQUERY([JRW-DS01],'EXECUTE AcquisitionCandidateAggregator.dbo.SFListingWithoutTenantAssociation WITH RESULT SETS (([Property__c] varchar(50),
[Tenant__c] varchar(50)
))')

-- select distinct x.*, v.SF_Tenant_Id
--  from(
-- select value
--	, Id as SF_Tenant_Id
-- from [SalesForce_Reporting].[dbo].[vTenant_ApprovedAggregator]


-- CROSS APPLY STRING_SPLIT(Aliases__c,';') 
--  where [Approved_Aggregator__c] = 1
--) as v (Aliases__c, SF_Tenant_Id)

--CROSS APPLY 
--( 
--select distinct SF_Listing.id as Property__c,
--v.SF_Tenant_Id as Tenant__c
--from dbo.vPropertyWithListingURL SF_Listing
--join AcquisitionCandidateAggregator.dbo.listing aca on SF_Listing.Listing_Website_Id__c = aca.ListingWebsiteId 
--																			and SF_Listing.Name = SUBSTRING(aca.[Name],0,80)
--																			--aca.ListingWebsiteId = SF_Listing.Listing_Website_Id__c
--join AcquisitionCandidateAggregator.dbo.ListingTenant lt on lt.ListingId = aca.id
----join dbo.vTenant_approvedAggregator SF_Tenant on SF_Tenant.Aliases__c = lt.TenantName
--left join dbo.vPropertyTenantAssociation vpta on vpta.Property__c = SF_Listing.id
--and vpta.Tenant__c = v.SF_Tenant_Id
--where vpta.id is null
--and (aca.name  like v.Aliases__c
--   or lt.TenantName like v.Aliases__c)

--   ) as x
GO
