SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO











CREATE VIEW [dbo].[vContactsDistinct] as 


select *
From openquery(SALESFORCEDEVART, 'select id, Email, Other_Email_Addresses__c, CRD__c, Broker_Dealer__c, FirstName, LastName, RecordTypeId, FINRA_Status__c from Contact')


GO
