SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









CREATE VIEW [dbo].[vContactsWithCRD] as 


select *
From openquery(SALESFORCEDEVART, 'select id, CRD__c, Broker_Dealer__c, FirstName, LastName, RecordTypeId, FINRA_Status__c from Contact where CRD__c is not null and CRD_Verified__c =1')


GO
