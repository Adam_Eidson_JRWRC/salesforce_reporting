SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE view [dbo].[vBrokerDealer] as 
Select *
From openquery(SALESFORCEDEVART, 'Select id, Name, Firm_CRD__c, Custodial_Platform__c From Broker_Dealer__c')
GO
