SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



















/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [dbo].[vPropertyToInsert] as 
with cte_nonTSCOListing as (
SELECT distinct SUBSTRING(Listing.[Name],0,80) as Name
      ,convert(varchar(15),Listing.ContractPrice) as Contract_Price__c
	  ,case when ContractPrice>0
	  then NOI/ContractPrice
	  end as CapRate
      --,[CapRate] as Current_Cap_Rate__c
     -- ,convert(varchar(15),Listing.noi) as tCurrent_NOI__c
	  ,convert(varchar(15),Listing.noi) as Purchase_NOI__c
	  ,convert(varchar(15),Listing.noi) as Purchase_NOI_Override__c
      ,convert(varchar(15),Listing.SqFt) as Sq_Ft__c
      --,cast([Years Remaining On Lease] as varchar(15)) as Current_Term_Remaining_years__c --formula field
     -- ,[Lot Size] as Lot_Size_Sq_Ft__c --formula field
      ,SUBSTRING(TRIM(Listing.[YearBuilt]),0,5) as Year_Built__c
      ,Listing.street as Street__c
      ,Listing.[city] as City__c
      ,case when len(Listing.[state]) > 2
	  then null
	  else Listing.[state] 
	  end as State__c
      ,cast(Listing.zip as varchar(50)) as Zip__c
      ,Listing.ListingURL as Listing_URL__c
     -- ,[BrokerWebsiteSource] as Seller_Brokerage__c
      --,[PropertyType] Field Needs to be created
      --,[PropertySubType] Field needs to be created
      ,Listing.TypeOfOwnership as Type_of_Ownership__c
      ,cast(Listing.[YearRenovated] as varchar(15)) as Year_Renovated__c
      --,[LeaseOptions]
      --,[TenantCredit]
      ,Listing.CurrentTermExpirationDate as Current_Term_Expiration_Date__c
	  ,Listing.NetLeaseType as NN_or_NNN__c
	  ,'Listing-Active' as Stage__c
	  --,'a1W0b000007SYyM' as Portfolio__c,
	  ,'a1W5a000008swKo'  as Portfolio__c,
	  vba.id as Seller_Agent__c,
	  vps.id  as Seller_Brokerage__c,
	  case when Listing.ListingCreatedDate = ''
	  then null
	  else convert(varchar(20),CAST(Listing.ListingCreatedDate as smalldatetime),126) 
	   end as Listing_Created_Date__c,
	   Listing.ListingWebsiteId as Listing_Website_Id__c,
	   convert(varchar(50),Listing.CrimeRateIndex) as Crime_Rate_Index__c,
	   Listing.Latitude as Address_Geocode__latitude__s,
	   Listing.Longitude as Address_Geocode__longitude__s,
	   case when isnull(Listing.ImprovedTenantCapture,0) = 1
	   Then 'true'
	   else 'false'
	   end as Improved_Tenant_Capture__c
  FROM [AcquisitionCandidateAggregator].[dbo].[vBrokerWebsiteDataApprovedTenant] Listing
  left join SalesForce_Reporting.dbo.vBrokerAgent vba on isnull(vba.firstName, '')+' '+isnull(vba.lastName, '') = isnull(Listing.[BrokerOfRecordName], '')
  --left join SalesForce_Reporting.dbo.vTenant SF_tenant on SF_tenant.Name = Listing.Tenant
  left join SalesForce_Reporting.dbo.vPropertyWithListingURL SF_property on SF_Property.Listing_Website_Id__c = Listing.ListingWebsiteId 
																			--and SF_Property.Name = SUBSTRING(Listing.[Name],0,80)--and SF_property.Listing_URL__c = Listing.ListingURL
  --and isNull(convert(varchar(15),Listing.ContractPrice),0) = SF_Property.Contract_Price__c
  left join SalesForce_Reporting.dbo.vPropertySource vps on isnull(vps.Name ,'') = isnull(Listing.sellerBrokerageName , '')
  --and SF_property.Listing_Website_Id__c = vps.id
  where SF_Property.id is null
  and Listing.Active = 1
  and (ISNULL([5MileRadiusPopulation],50000) >= 35000)
  --and vba.id is not null
  and listing.Name not like '%Tractor%'
  and (ISNULL(CrimeRateIndex,0)<=300)
  and listing.Name not like '%dark%'
  and listing.Name not like '%zero cash flow%'
  and listing.Name not like '%ZCF%'
  and listing.Name not like '%Outlot%'
  and listing.Name not like '%Outparcel%'
  and listing.Name not like '%Out parcel%'
  and listing.Name not like '%Carveout%'
  --and (DATEDIFF(year, GETDATE(),listing.CurrentTermExpirationDate) >= 5
  --or listing.CurrentTermExpirationDate is null)
  and (
  isnull(listing.[Mile1VacantHousingPercent],0) <= 15
  and isnull(listing.[Mile3VacantHousingPercent],0) <= 15
  and isnull(listing.[Mile5VacantHousingPercent],0) <= 15
  )
  and listing.SellerBrokerageId != 41--exclude JLL for now
  
), cte_TSCOListing as (
SELECT distinct SUBSTRING(Listing.[Name],0,80) as Name
       ,convert(varchar(15),Listing.ContractPrice) as Contract_Price__c
	   ,case when ContractPrice>0
	  then NOI/ContractPrice
	  end as CapRate
      --,[CapRate] as Current_Cap_Rate__c
      --,convert(varchar(15),Listing.noi) as tCurrent_NOI__c
	  ,convert(varchar(15),Listing.noi) as Purchase_NOI__c
	  ,convert(varchar(15),Listing.noi) as Purchase_NOI_Override__c
      ,convert(varchar(15),Listing.SqFt) as Sq_Ft__c
      --,cast([Years Remaining On Lease] as varchar(15)) as Current_Term_Remaining_years__c --formula field
     -- ,[Lot Size] as Lot_Size_Sq_Ft__c --formula field
      ,SUBSTRING(TRIM(Listing.[YearBuilt]),0,5) as Year_Built__c
      ,Listing.street as Street__c
      ,Listing.[city] as City__c
      ,case when len(Listing.[state]) > 2
	  then null
	  else Listing.[state] 
	  end as State__c
      ,cast(Listing.zip as varchar(50)) as Zip__c
      ,Listing.ListingURL as Listing_URL__c
     -- ,[BrokerWebsiteSource] as Seller_Brokerage__c
      --,[PropertyType] Field Needs to be created
      --,[PropertySubType] Field needs to be created
      ,Listing.TypeOfOwnership as Type_of_Ownership__c
      ,cast(Listing.[YearRenovated] as varchar(15)) as Year_Renovated__c
      --,[LeaseOptions]
      --,[TenantCredit]
      ,Listing.CurrentTermExpirationDate as Current_Term_Expiration_Date__c
	  ,Listing.NetLeaseType as NN_or_NNN__c
	  ,'Listing-Active' as Stage__c
	  --,'a1W0b000007SYyM' as Portfolio__c,
	  ,'a1W5a000008swKo'  as Portfolio__c,
	  vba.id as Seller_Agent__c,
	  vps.id  as Seller_Brokerage__c,
	  case when Listing.ListingCreatedDate = ''
	  then null
	  else convert(varchar(20),CAST(Listing.ListingCreatedDate as smalldatetime),126) 
	   end as Listing_Created_Date__c,
	   Listing.ListingWebsiteId as Listing_Website_Id__c,
	   convert(varchar(50),Listing.CrimeRateIndex) as Crime_Rate_Index__c,
	   Listing.Latitude as Address_Geocode__latitude__s,
	   Listing.Longitude as Address_Geocode__longitude__s,
	   case when isnull(Listing.ImprovedTenantCapture,0) = 1
	   Then 'true'
	   else 'false'
	   end as Improved_Tenant_Capture__c
  FROM [AcquisitionCandidateAggregator].[dbo].[vBrokerWebsiteDataApprovedTenant] Listing
  left join SalesForce_Reporting.dbo.vBrokerAgent vba on isnull(vba.firstName, '') +' '+isnull(vba.lastName, '')  = cast(isnull(Listing.[BrokerOfRecordName], '') as varchar(50))
  --left join SalesForce_Reporting.dbo.vTenant SF_tenant on SF_tenant.Name = Listing.Tenant
  left join SalesForce_Reporting.dbo.vPropertyWithListingURL SF_property on SF_Property.Listing_Website_Id__c = Listing.ListingWebsiteId
																			--and SF_Property.Name = SUBSTRING(Listing.[Name],0,80)
  left join SalesForce_Reporting.dbo.vPropertySource vps on isnull(vps.Name ,'') = isnull(Listing.sellerBrokerageName , '')
  where SF_Property.id is null
  and Listing.Active = 1
  and (ISNULL([5MileRadiusPopulation],50000) >= 30000)
  --and vba.id is not null
  and Listing.Name like '%Tractor%'
  and (ISNULL(CrimeRateIndex,0)<=300)
  and listing.Name not like '%dark%'
  and listing.Name not like '%zero cash flow%'
  and listing.Name not like '%Outlot%'
  and listing.Name not like '%Outparcel%'
  and listing.Name not like '%Out parcel%'
  and listing.Name not like '%Carveout%'
  --and (DATEDIFF(year, GETDATE(),listing.CurrentTermExpirationDate) >= 5
  --or listing.CurrentTermExpirationDate is null)
  and (
  isnull(listing.[Mile1VacantHousingPercent],0) <= 15
  and isnull(listing.[Mile3VacantHousingPercent],0) <= 15
  and isnull(listing.[Mile5VacantHousingPercent],0) <= 15
  )
  and listing.SellerBrokerageId != 41--exclude JLL for now
), cte_finalPrepped as (


select distinct *
from cte_nonTSCOListing
where ISNULL(CapRate,6)>=0.0475
or ISNULL(CapRate,6)=0
union
select distinct *
from cte_TSCOListing
where ISNULL(CapRate,6)>=0.0475
or ISNULL(CapRate,6)=0
), cte_RowNumber as (
--next, get top 1 record where multiple listing agents are identified.

SELECT distinct Name
       ,Contract_Price__c
      --,[CapRate] as Current_Cap_Rate__c
     -- ,tCurrent_NOI__c
	  ,Purchase_NOI__c
	  ,Purchase_NOI_Override__c
      ,Sq_Ft__c
      --,cast([Years Remaining On Lease] as varchar(15)) as Current_Term_Remaining_years__c --formula field
     -- ,[Lot Size] as Lot_Size_Sq_Ft__c --formula field
      ,Year_Built__c
      ,Street__c
      ,City__c
      ,State__c
      ,Zip__c
      ,Listing_URL__c
     -- ,[BrokerWebsiteSource] as Seller_Brokerage__c
      --,[PropertyType] Field Needs to be created
      --,[PropertySubType] Field needs to be created
      ,Type_of_Ownership__c
      ,Year_Renovated__c
      --,[LeaseOptions]
      --,[TenantCredit]
      ,convert(varchar(50),Current_Term_Expiration_Date__c) as Current_Term_Expiration_Date__c
	  ,NN_or_NNN__c
	  ,Stage__c
	  ,Portfolio__c,
	 Seller_Agent__c,
	  Seller_Brokerage__c,
	  convert(varchar(50),Listing_Created_Date__c) as Listing_Created_Date__c,
	   Listing_Website_Id__c,
	   Crime_Rate_Index__c,
	   Address_Geocode__latitude__s,
	   Address_Geocode__longitude__s,
	   Improved_Tenant_Capture__c,
	   ROW_NUMBER() over ( Partition By Listing_Website_Id__c Order by Seller_Agent__c, Improved_Tenant_Capture__c asc) as rownum
	   from cte_finalPrepped
	   )
	   select distinct Name
       ,Contract_Price__c
      --,[CapRate] as Current_Cap_Rate__c
      --,tCurrent_NOI__c
	  ,Purchase_NOI__c
	  ,Purchase_NOI_Override__c
      ,Sq_Ft__c
      --,cast([Years Remaining On Lease] as varchar(15)) as Current_Term_Remaining_years__c --formula field
     -- ,[Lot Size] as Lot_Size_Sq_Ft__c --formula field
      ,Year_Built__c
      ,Street__c
      ,City__c
      ,State__c
      ,Zip__c
      ,Listing_URL__c
     -- ,[BrokerWebsiteSource] as Seller_Brokerage__c
      --,[PropertyType] Field Needs to be created
      --,[PropertySubType] Field needs to be created
      ,Type_of_Ownership__c
      ,Year_Renovated__c
      --,[LeaseOptions]
      --,[TenantCredit]
      ,Current_Term_Expiration_Date__c
	  ,NN_or_NNN__c
	  ,Stage__c
	  ,Portfolio__c,
	 Seller_Agent__c,
	  Seller_Brokerage__c,
	  Listing_Created_Date__c,
	   Listing_Website_Id__c,
	   Crime_Rate_Index__c,
	   Improved_Tenant_Capture__c,
	   convert(varchar(15),cast(Address_Geocode__latitude__s as DECIMAL(12,9))) as Address_Geocode__Latitude__s,
	   convert(varchar(15), cast(Address_Geocode__longitude__s as DECIMAL(12,9))) as Address_Geocode__Longitude__s,
	   'Aggregator: Property Listing' as Property_Origin__c
	   from cte_RowNumber 
	   where rownum = 1
GO
