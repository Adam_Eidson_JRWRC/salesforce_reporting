SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE View [SF_FULLCOPY].[vBrokerAgent] as 
with cte_BA as (
Select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'Select ID, RecordTypeId, FirstName, LastName, email, Title from Contact')
), cte_PropertySourceRecordType as (
select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'select rt.Id, rt.Name  from Contact c
join RecordType rt on rt.id = c.RecordTypeId
where rt.name in (''Property Source'') group by rt.Id, rt.name')
)

select ba.*
From cte_PropertySourceRecordType rt
join cte_BA ba on ba.RecordTypeId = rt.Id
GO
