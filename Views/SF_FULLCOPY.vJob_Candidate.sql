SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE view [SF_FULLCOPY].[vJob_Candidate] as 


select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'select Email__c, Id, name, Status__c from Job_Candidate__c')
GO
