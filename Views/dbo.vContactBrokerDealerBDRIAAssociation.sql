SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create View [dbo].[vContactBrokerDealerBDRIAAssociation] as
with cte_Contact as (
Select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'Select Broker_Dealer__c, id, CRD__c from Contact where CRD__c is not null')
), cte_Broker_Dealer as (
select *
from openquery(SALESFORCEDEVARTFULLCOPY, 'Select id, name, Firm_CRD__c from Broker_Dealer__c')
), cte_BD_RIA_Association__c as (
select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'select id, Contact__c, Broker_Dealer__c from BD_RIA_Association__c')
)

select distinct cc.id, 
cc.CRD__c,
cbd.name as FirmName,
cas.id as BDRIAAssociationID
From cte_Contact cc
join cte_Broker_Dealer cbd on cc.Broker_Dealer__c = cbd.id
join cte_BD_RIA_Association__c cas on cas.Contact__c = cc.id
GO
