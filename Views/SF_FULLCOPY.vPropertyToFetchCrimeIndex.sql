SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [SF_FULLCOPY].[vPropertyToFetchCrimeIndex] as 

with cte_NonTSCOApprovedTenant as (
SELECT distinct SUBSTRING([listingName],0,80) as Name
      ,REPLACE(REPLACE([ListingPrice],'$',''),',','') as Contract_Price__c
      --,[CapRate] as Current_Cap_Rate__c
      ,[noi] as tCurrent_NOI__c
	  , [noi] as Purchase_NOI__c
      ,cast(REPLACE([Gross SF] ,',','')as float) as Sq_Ft__c
      --,[Years Remaining On Lease] as Current_Term_Remaining_years__c
      --,[Lot Size] as Lot_Size_Sq_Ft__c
      ,SUBSTRING(TRIM([Year Built]),0,5) as Year_Built__c
      ,[address] as Street__c
      ,[city] as City__c
      ,[state] as State__c
      ,[PostalCode] as Zip__c
      ,[PropertyURL] as Listing_URL__c
      --,[BrokerWebsiteSource] as Seller_Brokerage__c
      --,[PropertyType] Field Needs to be created
      --,[PropertySubType] Field needs to be created
      ,[Ownership] as Type_of_Ownership__c
      ,[YearRenovated] as Year_Renovated__c
      --,[LeaseOptions]
      --,[TenantCredit]
      ,case when [LeaseExpiration] = ''
	  then null
	  else convert(char(10),CAST([LeaseExpiration] as Date),120) 
	  end as Current_Term_Expiration_Date__c
	  ,[LeaseType] as NN_or_NNN__c
	  ,[Mile1TotalPopulation] as X1_Mile_Radius_Population__c
      ,[Mile1MedianHHIncome] as Median_Household_Income__c
      ,[Mile1VACANTHousingUnit] as X1_Mile_Radius_Vacant_Housing__c
      ,[Mile1OwnerOccupiedHousingUnit] as X1_Mile_Radius_Owner_Occupied_Housing__c
      ,[Mile1RentedHousingUnit] as X1_Mile_Radius_Renter_Occupied_Housing__c
      ,[Mile1AverageHHIncome] as X1_Mile_Radius_Avg_HH_Income__c
      ,[Mile3TotalPopulation] as X3_Mile_Radius_Population__c
      ,[Mile3VACANTHousingUnit] as X3_Mile_Radius_Vacant_Housing__c
      ,[Mile3OwnerOccupiedHousingUnit] as X3_Mile_Radius_Owner_Occupied_Housing__c
      ,[Mile3RentedHousingUnit] as X3_Mile_Radius_Renter_Occupied_Housing__c
      ,[Mile3AverageHHIncome] as X3_Mile_Radius_Avg_HH_Income__c
      ,[Mile5TotalPopulation] as X5_Mile_Radius_Population__c
      ,[Mile5VACANTHousingUnit] as X5_Mile_Radius_Vacant_Housing__c
      ,[Mile5OwnerOccupiedHousingUnit] as X5_Mile_Radius_Owner_Occupied_Housing__c
      ,[Mile5RentedHousingUnit] as X5_Mile_Radius_Renter_Occupied_Housing__c
	  ,'Listing-Active' as Stage__c
	  ,'a1W0m000001GNcW' as Portfolio__c,
	  vba.id as Seller_Agent__c,
	  vps.id as Seller_Brokerage__c,
	  case when Listing.ListingDatetime = ''
	  then null
	  else convert(varchar(20),CAST(Listing.ListingDatetime as smalldatetime),126) 
	   end as Listing_Created_Date__c,
	   Listing.BrokerWebsiteId as Listing_Website_Id__c
  FROM [AcquisitionCandidateAggregator].[dbo].[vBrokerWebsiteDataApprovedTenant] Listing
  left join [AcquisitionCandidateAggregator].[dbo].Demographic d on d.BrokerWebsiteId= Listing.BrokerWebsiteId
  left join SalesForce_Reporting.SF_FULLCOPY.vBrokerAgent vba on vba.firstName+' '+vba.lastName = Listing.[BrokerOfRecordName]
  --left join SalesForce_Reporting.SF_FULLCOPY.vTenant SF_tenant on SF_tenant.Name = Listing.Tenant
  left join SalesForce_Reporting.SF_FULLCOPY.vPropertyWithListingURL SF_property on REPLACE(REPLACE(Listing.PropertyURL,CHAR(13),''), CHAR(10),'') = REPLACE(REPLACE(SF_Property.Listing_URL__c,CHAR(13),''), CHAR(10),'')
  or SF_Property.Listing_Website_Id__c = Listing.BrokerWebsiteId
  left join SalesForce_Reporting.SF_FULLCOPY.vPropertySource vps on vps.Name = Listing.BrokerWebsiteSource
  where SF_Property.id is null
  and (Mile3TotalPopulation  >= 40000
  OR Mile5TotalPopulation >= 40000
  or Mile5TotalPopulation is null)
  --and vba.id is not null
  and listing.Tenant  not like '%tractor%'
), cte_TSCOApprovedTenant as (
SELECT distinct SUBSTRING([listingName],0,80) as Name
      ,REPLACE(REPLACE([ListingPrice],'$',''),',','') as Contract_Price__c
      --,[CapRate] as Current_Cap_Rate__c
      ,[noi] as tCurrent_NOI__c
	  , [noi] as Purchase_NOI__c
      ,cast(REPLACE([Gross SF] ,',','')as float) as Sq_Ft__c
      --,[Years Remaining On Lease] as Current_Term_Remaining_years__c
      --,[Lot Size] as Lot_Size_Sq_Ft__c
      ,SUBSTRING(TRIM([Year Built]),0,5) as Year_Built__c
      ,[address] as Street__c
      ,[city] as City__c
      ,[state] as State__c
      ,[PostalCode] as Zip__c
      ,[PropertyURL] as Listing_URL__c
      --,[BrokerWebsiteSource] as Seller_Brokerage__c
      --,[PropertyType] Field Needs to be created
      --,[PropertySubType] Field needs to be created
      ,[Ownership] as Type_of_Ownership__c
      ,[YearRenovated] as Year_Renovated__c
      --,[LeaseOptions]
      --,[TenantCredit]
      ,case when [LeaseExpiration] = ''
	  then null
	  else convert(char(10),CAST([LeaseExpiration] as Date),120) 
	  end as Current_Term_Expiration_Date__c
	  ,[LeaseType] as NN_or_NNN__c
	  ,[Mile1TotalPopulation] as X1_Mile_Radius_Population__c
      ,[Mile1MedianHHIncome] as Median_Household_Income__c
      ,[Mile1VACANTHousingUnit] as X1_Mile_Radius_Vacant_Housing__c
      ,[Mile1OwnerOccupiedHousingUnit] as X1_Mile_Radius_Owner_Occupied_Housing__c
      ,[Mile1RentedHousingUnit] as X1_Mile_Radius_Renter_Occupied_Housing__c
      ,[Mile1AverageHHIncome] as X1_Mile_Radius_Avg_HH_Income__c
      ,[Mile3TotalPopulation] as X3_Mile_Radius_Population__c
      ,[Mile3VACANTHousingUnit] as X3_Mile_Radius_Vacant_Housing__c
      ,[Mile3OwnerOccupiedHousingUnit] as X3_Mile_Radius_Owner_Occupied_Housing__c
      ,[Mile3RentedHousingUnit] as X3_Mile_Radius_Renter_Occupied_Housing__c
      ,[Mile3AverageHHIncome] as X3_Mile_Radius_Avg_HH_Income__c
      ,[Mile5TotalPopulation] as X5_Mile_Radius_Population__c
      ,[Mile5VACANTHousingUnit] as X5_Mile_Radius_Vacant_Housing__c
      ,[Mile5OwnerOccupiedHousingUnit] as X5_Mile_Radius_Owner_Occupied_Housing__c
      ,[Mile5RentedHousingUnit] as X5_Mile_Radius_Renter_Occupied_Housing__c
	  ,'Listing-Active' as Stage__c
	  ,'a1W0m000001GNcW' as Portfolio__c,
	  vba.id as Seller_Agent__c,
	  vps.id as Seller_Brokerage__c,
	  case when Listing.ListingDatetime = ''
	  then null
	  else convert(varchar(20),CAST(Listing.ListingDatetime as smalldatetime),126) 
	   end as Listing_Created_Date__c,
	   Listing.BrokerWebsiteId as Listing_Website_Id__c
  FROM [AcquisitionCandidateAggregator].[dbo].[vBrokerWebsiteDataApprovedTenant] Listing
  left join [AcquisitionCandidateAggregator].[dbo].Demographic d on d.BrokerWebsiteId= Listing.BrokerWebsiteId
  left join SalesForce_Reporting.SF_FULLCOPY.vBrokerAgent vba on vba.firstName+' '+vba.lastName = Listing.[BrokerOfRecordName]
  --left join SalesForce_Reporting.SF_FULLCOPY.vTenant SF_tenant on SF_tenant.Name = Listing.Tenant
  left join SalesForce_Reporting.SF_FULLCOPY.vPropertyWithListingURL SF_property on REPLACE(REPLACE(Listing.PropertyURL,CHAR(13),''), CHAR(10),'') = REPLACE(REPLACE(SF_Property.Listing_URL__c,CHAR(13),''), CHAR(10),'')
  or SF_Property.Listing_Website_Id__c = Listing.BrokerWebsiteId
  left join SalesForce_Reporting.SF_FULLCOPY.vPropertySource vps on vps.Name = Listing.BrokerWebsiteSource
  where SF_Property.id is null
  and (Mile3TotalPopulation  >= 30000
  OR Mile5TotalPopulation >= 30000
  or Mile5TotalPopulation is null)
  --and vba.id is not null
  and listing.Tenant   like '%tractor%'
)

select *
From cte_NonTSCOApprovedTenant
union
select *
from cte_TSCOApprovedTenant

GO
