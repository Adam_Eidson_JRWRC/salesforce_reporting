SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [dbo].[vPropertyToFetchCrimeRate] as 
select distinct p.Id
--[Mile1TotalPopulation]  as  X1_Mile_Radius_Population__c
--      ,[Mile1MedianHHIncome] as Median_Household_Income__c
--      ,[Mile1VACANTHousingUnit] as X1_Mile_Radius_Vacant_Housing__c
--      ,[Mile1OwnerOccupiedHousingUnit] as X1_Mile_Radius_Owner_Occupied_Housing__c
--      ,[Mile1RentedHousingUnit] as X1_Mile_Radius_Renter_Occupied_Housing__c
--      ,[Mile1AverageHHIncome] as X1_Mile_Radius_Avg_HH_Income__c
--      ,[Mile3TotalPopulation]as X3_Mile_Radius_Population__c
--      ,[Mile3VACANTHousingUnit]as X3_Mile_Radius_Vacant_Housing__c
--      ,[Mile3OwnerOccupiedHousingUnit] as X3_Mile_Radius_Owner_Occupied_Housing__c
--      ,[Mile3RentedHousingUnit] as X3_Mile_Radius_Renter_Occupied_Housing__c
--      ,[Mile3AverageHHIncome] as X3_Mile_Radius_Avg_HH_Income__c
--      ,[Mile5TotalPopulation] as X5_Mile_Radius_Population__c
--      ,[Mile5VACANTHousingUnit] as X5_Mile_Radius_Vacant_Housing__c
--      ,[Mile5OwnerOccupiedHousingUnit]as X5_Mile_Radius_Owner_Occupied_Housing__c
--      ,[Mile5RentedHousingUnit] as X5_Mile_Radius_Renter_Occupied_Housing__c
--	  ,CrimeIndex as Crime_Rate_Index__c
	  ,p.street__c
	  ,p.city__c
	  ,p.state__c
	  ,p.zip__c
	  ,d.Latitude
	  ,d.Longitude
From SalesForce_Reporting.dbo.vPropertyWithListingURL p
join AcquisitionCandidateAggregator.dbo.listing d on d.listingWebsiteId=  p.Listing_Website_Id__c
where p.X5_Mile_Radius_Population__c >40000
AND P.CRIME_RATE_index__c is null
GO
