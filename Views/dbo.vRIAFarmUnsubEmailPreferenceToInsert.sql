SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [dbo].[vRIAFarmUnsubEmailPreferenceToInsert] as 
SELECT  distinct vcd.id as Contact__c,
'Category' as Level__c,
'ER Industry (BD/RIA Contacts)' as Subscription__c,
cast('True' as bit) as Do_Not_Send__c,
'User Opt-Out' as Reason_Type__c,
'Unsubscribed from RIA Farm' as Reason_Description__c
  FROM [MailApp].[STAGE].[CampaignEngagement-RIAFarm] rf
   join SalesForce_Reporting.dbo.vContactsDistinct vcd on vcd.Email = rf.ToEmailAddress
   left join openquery(SALESFORCEDEVART,'select Contact__c from Email_Preference__c where Level__c = ''Category'' and Subscription__c = ''ER Industry (BD/RIA Contacts)''') SF_EP on SF_EP.Contact__c = vcd.id
  where (Unsubscribed != 0)
  and SF_EP.Contact__c is null
  
GO
