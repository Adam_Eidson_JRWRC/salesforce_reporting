SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [SF_FULLCOPY].[vPropertySource] as 
select *
from openquery(SALESFORCEDEVARTFULLCOPY, 'Select id, Name from Property_Source__c')
GO
