SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE view [dbo].[vPropertyToUpdateNOI] as 
select sf.Id,
convert(varchar(50), l.NOI ) as Purchase_NOI__c
from SalesForce_Reporting.dbo.vProperty sf
join AcquisitionCandidateAggregator.dbo.Listing l on l.ListingWebsiteId = sf.Listing_Website_Id__c
where (sf.tCurrent_NOI__c is null
or sf.Purchase_NOI__c is null)
and l.SellerBrokerageId  = 10
and NOI is not null
GO
