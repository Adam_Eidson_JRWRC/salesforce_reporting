SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE View [dbo].[vClientsWithNonCancelledTrades] as
select sf.id as ClientId,
vt.Status__c as TradeStatus,
Age1__c as Age1,
In_A_1031_Transaction__c,
X1031_Interest__pc,
X1031_Producer_2__pc,
X1031_45th_Day__c,
Lead_Created_Date__c,
CreatedDate,
DateDiff(DAY,CreatedDate,X1031_45th_Day__c)+15 as [CreatedTill60thDay],
DateDiff(DAY,Lead_Created_Date__c,X1031_45th_Day__c)+15 as [LeadCreatedTill60thDay]
From openquery(SALESFORCEDEVART,'select id, RecordTypeId, Age1__c, In_A_1031_Transaction__c, X1031_Interest__pc, X1031_Producer_2__pc, X1031_45th_Day__c, CreatedDate, Lead_Created_Date__c from Account') sf
join SalesForce_Reporting.dbo.vClientRecordType vcrt on vcrt.id = sf.RecordTypeId
join SalesForce_Reporting.dbo.vTrade vt on vt.Client_Name__c = sf.id
where vt.Status__c != 'Cancelled'
GO
