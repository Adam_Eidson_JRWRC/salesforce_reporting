SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



Create VIEW [SF_FULLCOPY].[vContactsWithCRDNotAtFirmsToSyncWithFINRA] as 
with cte_FirmsWSellingAgreement as (
select * 
from openquery(SALESFORCEDEVARTFULLCOPY, 'Select id, Firm_CRD__c, Signed_Selling_Agreement__c, Key_Account__c from Broker_Dealer__c')
),cte_Firms as (
select * 
from openquery(SALESFORCEDEVARTFULLCOPY, 'Select id, Firm_CRD__c, All_Signed_Selling_Agreements__c from Broker_Dealer__c where Firm_CRD__c != null ')
), cte_Investors as (
select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'select id, Broker_Dealer_ER__c from Account where Broker_Dealer_ER__c != null and stage__c = ''Closed and Funded''')
), cte_Contact as (

select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'select id, CRD__c, Broker_Dealer__c from Contact where CRD__c is not null')

), cte_ContactsToSync as (

select cast(Firm_CRD__c as varchar(50)) as Firm_CRD__c,
cc.CRD__c
From cte_FirmsWSellingAgreement cfsa
join cte_Contact cc on cc.Broker_Dealer__c = cfsa.id
where (key_Account__c = 1
or signed_Selling_Agreement__c = 1)
and Firm_CRD__c not in ('133763',
'10299',
'6363',
'7072',
'8158',
'13686',
'7870',
'35747',
'6413')
union
select cast(cf.Firm_CRD__c as varchar(50)) as Firm_CRD__c,
cc.CRD__c
from cte_Firms cf
join cte_Investors ci on ci.Broker_Dealer_ER__c = cf.id
join cte_Contact cc on cc.Broker_Dealer__c = cf.id
where  Firm_CRD__c not in ('133763',
'10299',
'6363',
'7072',
'8158',
'13686',
'7870',
'35747',
'6413')
)
select cc.CRD__c, cc.id
from cte_Contact cc
left join cte_Firms cf on cf.id = cc.Broker_Dealer__c 
left join cte_ContactsToSync cctc on cctc.CRD__c = cc.CRD__c
where cctc.CRD__c is null
and isnull(cf.Firm_CRD__c, '') not in ('133763',
'10299',
'6363',
'7072',
'8158',
'13686',
'7870',
'35747',
'6413')
GO
