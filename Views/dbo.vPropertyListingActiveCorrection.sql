SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [dbo].[vPropertyListingActiveCorrection] as 
select distinct vpl.id,
'Listing-Active' as Stage__c
From SalesForce_Reporting.dbo.vPropertyWithListingURL vpl
left join AcquisitionCandidateAggregator.dbo.Listing vbd on vpl.Listing_Website_Id__c = vbd.ListingWebsiteId --.Listing_URL__c = REPLACE(REPLACE(vbd.ListingURL,CHAR(13), ''), CHAR(10),'')
where vbd.Active = 1
and vpl.Stage__c = 'Listing-Expired'
GO
