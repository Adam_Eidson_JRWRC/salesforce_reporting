SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [SF_FULLCOPY].[vBDRIAAssociationRecordType] as 
select *
From openquery(SalesforceDevartFULLCOPY, 'select rt.Id, rt.Name  from BD_RIA_Association__c c
join RecordType rt on rt.id = c.RecordTypeId
group by rt.Id, rt.name')
GO
