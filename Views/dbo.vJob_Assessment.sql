SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vJob_Assessment] as 

select *
From openquery(SALESFORCEDEVART, 'select Id, Candidate__c,Employee__c,  RecordTypeId, Submission_ID__c , createdDate from Job_Assessment__c')
GO
