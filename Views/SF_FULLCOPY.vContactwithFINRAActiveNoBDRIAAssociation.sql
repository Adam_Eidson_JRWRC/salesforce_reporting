SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--Contacts with FINRA Status = active and zero BD/RIA Association records
CREATE VIEW [SF_FULLCOPY].[vContactwithFINRAActiveNoBDRIAAssociation] as 
with cte_Contact as (
select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'select id, CRD__c, FINRA_Status__c, Broker_Dealer__c from Contact where CRD__c is not null')
), cte_BDRIAAssociation as (
select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'select id, Broker_Dealer__c, Contact__c, CreatedDate, Active__c from BD_RIA_Association__c')
)

select cc.*
From cte_Contact cc
left join cte_BDRIAAssociation cba on cc.id = cba.Contact__c

where cba.id is null
and FINRA_Status__c = 'Active'
GO
