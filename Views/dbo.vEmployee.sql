SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vEmployee] as 

select *
From openquery(SALESFORCEDEVART, 'select Email__c, Id, Name, Status__c from Employee__c')
GO
