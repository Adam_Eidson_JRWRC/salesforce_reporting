SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



  CREATE view [SF_FULLCOPY].[vPropertyToUpdateListingWebsiteId] as 
  select distinct --d.CrimeIndex as Crime_Rate_Index__c,
  aca.BrokerWebsiteId as Listing_Website_Id__c,
  sf_prop.id
  From AcquisitionCandidateAggregator.dbo.vBrokerWebsiteDataApprovedTenant aca
  left join [AcquisitionCandidateAggregator].[dbo].[Demographic] d on d.BrokerWebsiteId= aca.BrokerWebsiteId
  left join SalesForce_Reporting.SF_FULLCOPY.vPropertyWithListingURL SF_prop  on REPLACE(REPLACE(aca.PropertyURL,CHAR(13),''), CHAR(10),'') = REPLACE(REPLACE(SF_Prop.Listing_URL__c,CHAR(13),''), CHAR(10),'')
  where id is not null
  --and d.CrimeIndex is not null
  and sf_prop.Listing_Website_Id__c is null
  and aca.BrokerWebsiteId is not null
GO
