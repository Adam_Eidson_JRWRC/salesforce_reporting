SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vSFAccountInvestmentPrefsExperience] as 
with cte_SFAccount as (
select id, 
Accredited__c,
Individual_Investments_Allow_Higher_Risk__c,
Liquidity_Needs__c,
Allows_for_Speculation__c, 
Time_Horizon__c,
isnull(cast(Investment_Objective__c as varchar(150)), '') as Investment_Objective__c , 
Inv_Category_Preferences__c, 
Asset_Class_Preferences__c, 
Stocks__c, 
Bonds__c, 
Options__c, 
Mutual_Funds__c, 
DPP__c, 
REITs__c, 
Annuities__c, 
Investment_Real_Estate__c, 
 Alternatives__c 
From openquery(SALESFORCEDEVART, 'Select id, Accredited__c, Individual_Investments_Allow_Higher_Risk__c, Liquidity_Needs__c,Allows_for_Speculation__c, Time_Horizon__c,Investment_Objective__c, Inv_Category_Preferences__c, Asset_Class_Preferences__c, Stocks__c, Bonds__c, Options__c, Mutual_Funds__c, DPP__c, REITs__c, Annuities__c, Investment_Real_Estate__c, Alternatives__c from Account')
)
select *
from cte_SFAccount
cross apply string_split(investment_Objective__c,';') 
GO
