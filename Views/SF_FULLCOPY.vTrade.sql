SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [SF_FULLCOPY].[vTrade] as 
select *
From openquery(SalesforceDevartFULLCOPY, 'select Client_Name__c, Id, status__c from Trade__c')
GO
