SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

















CREATE VIEW [dbo].[vPropertyWithListingURL] as 
Select *
From openquery(SALESFORCEDEVART, 'Select Id, Name,X5_Mile_Radius_Population__c, CRIME_RATE_index__c, Address_Geocode__latitude__s, Address_Geocode__longitude__s, Purchase_NOI__c, Listing_URL__c, Stage__c, Purchase_Price__c,Contract_Price__c, Listing_Website_Id__c,Seller_Brokerage__c, Street__c, City__c, State__c,  Zip__c, Property_Source__c, createdDate, Reason_For_Rejection__c from Property__c where Listing_URL__c is not null and Listing_Website_Id__c is not null')
GO
