SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vJRWRealtyLaunchLeadProperty] as 
select sf.id, 
case when MailingStreet like '%ste %'
then 
SUBSTRING(MailingStreet,0,CHARINDEX('Ste ',MailingStreet)) 
when MailingStreet like '%suite%'
then 
SUBSTRING(MailingStreet,0,CHARINDEX('suite',MailingStreet)) 
else MailingStreet
end as MailingStreet,
MailingCity,
MailingState,
MailingPostalCode
From openquery(SALESFORCEDEVART,'Select Id, AccountId,MailingLatitude, MailingLongitude, MailingStreet , MailingCity, MailingState, MailingPostalCode from Contact where AccountId = ''0010b00002dgnqJ''') sf
left join AcquisitionCandidateAggregator.CallCenter.Demographic d on d.SFID = sf.id
where d.id is null

--JRW Realty Launch Leads

--JRW Realty AnswerPro
GO
