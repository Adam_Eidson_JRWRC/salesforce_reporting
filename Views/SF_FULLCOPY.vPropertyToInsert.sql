SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

























/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [SF_FULLCOPY].[vPropertyToInsert] as 
with cte_nonTSCOListing as (
SELECT distinct SUBSTRING(Listing.[Name],0,80) as Name
      ,convert(varchar(15),Listing.ContractPrice) as Contract_Price__c
	  ,case when ContractPrice>0
	  then NOI/ContractPrice
	  end as CapRate
      --,[CapRate] as Current_Cap_Rate__c
      ,convert(varchar(15),Listing.noi) as tCurrent_NOI__c
	  ,convert(varchar(15),Listing.noi) as Purchase_NOI__c
      ,convert(varchar(15),Listing.SqFt) as Sq_Ft__c
      --,cast([Years Remaining On Lease] as varchar(15)) as Current_Term_Remaining_years__c --formula field
     -- ,[Lot Size] as Lot_Size_Sq_Ft__c --formula field
      ,SUBSTRING(TRIM(Listing.[YearBuilt]),0,5) as Year_Built__c
      ,Listing.street as Street__c
      ,Listing.[city] as City__c
      ,case when len(Listing.[state]) > 2
	  then null
	  else Listing.[state] 
	  end as State__c
      ,cast(Listing.zip as varchar(50)) as Zip__c
      ,Listing.ListingURL as Listing_URL__c
     -- ,[BrokerWebsiteSource] as Seller_Brokerage__c
      --,[PropertyType] Field Needs to be created
      --,[PropertySubType] Field needs to be created
      ,Listing.TypeOfOwnership as Type_of_Ownership__c
      ,cast(Listing.[YearRenovated] as varchar(15)) as Year_Renovated__c
      --,[LeaseOptions]
      --,[TenantCredit]
      ,Listing.CurrentTermExpirationDate as Current_Term_Expiration_Date__c
	  ,Listing.NetLeaseType as NN_or_NNN__c
	  ,convert(varchar(50),[1MileRadiusPopulation]) as X1_Mile_Radius_Population__c
      ,convert(varchar(50),[MedianHouseholdIncome]) as Median_Household_Income__c
      ,convert(varchar(50),[1MileRadiusVacantHousing]) as X1_Mile_Radius_Vacant_Housing__c
      ,convert(varchar(50),[1MileRadiusOwnerOccupiedHousing]) as X1_Mile_Radius_Owner_Occupied_Housing__c
      ,convert(varchar(50),[1MileRadiusRenterOccupiedHousing]) as X1_Mile_Radius_Renter_Occupied_Housing__c
      ,convert(varchar(50),[1MileRadiusAvgHHIncome]) as X1_Mile_Radius_Avg_HH_Income__c
      ,convert(varchar(50),[3MileRadiusPopulation]) as X3_Mile_Radius_Population__c
      ,convert(varchar(50),[3MileRadiusVacantHousing]) as X3_Mile_Radius_Vacant_Housing__c
      ,convert(varchar(50),[3MileRadiusOwnerOccupiedHousing]) as X3_Mile_Radius_Owner_Occupied_Housing__c
      ,convert(varchar(50),[3MileRadiusRenterOccupiedHousing]) as X3_Mile_Radius_Renter_Occupied_Housing__c
      ,convert(varchar(50),[3MileRadiusAvgHHIncome]) as X3_Mile_Radius_Avg_HH_Income__c
      ,convert(varchar(50),[5MileRadiusPopulation]) as X5_Mile_Radius_Population__c
      ,convert(varchar(50),[5MileRadiusVacantHousing]) as X5_Mile_Radius_Vacant_Housing__c
      ,convert(varchar(50),[5MileRadiusOwnerOccupiedHousing]) as X5_Mile_Radius_Owner_Occupied_Housing__c
      ,convert(varchar(50),[5MileRadiusRenterOccupiedHousing]) as X5_Mile_Radius_Renter_Occupied_Housing__c
	  ,'Listing-Active' as Stage__c
	  ,'a1W0b000007SYyM' as Portfolio__c,
	  vba.id as Seller_Agent__c,
	  vps.id  as Seller_Brokerage__c,
	  case when Listing.ListingCreatedDate = ''
	  then null
	  else convert(varchar(20),CAST(Listing.ListingCreatedDate as smalldatetime),126) 
	   end as Listing_Created_Date__c,
	   Listing.ListingWebsiteId as Listing_Website_Id__c,
	   convert(varchar(50),Listing.CrimeRateIndex) as Crime_Rate_Index__c
  FROM [AcquisitionCandidateAggregator].[dbo].[vBrokerWebsiteDataApprovedTenant] Listing
  left join SalesForce_Reporting.SF_FULLCOPY.vBrokerAgent vba on vba.firstName+' '+vba.lastName = Listing.[BrokerOfRecordName]
  --left join SalesForce_Reporting.dbo.vTenant SF_tenant on SF_tenant.Name = Listing.Tenant
  left join SalesForce_Reporting.SF_FULLCOPY.vPropertyWithListingURL SF_property on SF_Property.Listing_Website_Id__c = Listing.ListingWebsiteId 
																			--and SF_Property.Name = SUBSTRING(Listing.[Name],0,80)--and SF_property.Listing_URL__c = Listing.ListingURL
  --and isNull(convert(varchar(15),Listing.ContractPrice),0) = SF_Property.Contract_Price__c
  left join SalesForce_Reporting.dbo.vPropertySource vps on cast(isnull(vps.Name ,'')as varchar(50)) = cast(isnull(Listing.sellerBrokerageName , '')as varchar(50))
  --and SF_property.Listing_Website_Id__c = vps.id
  where SF_Property.id is null
  and Listing.Active = 1
  and (ISNULL([5MileRadiusPopulation],50000) >= 40000)
  --and vba.id is not null
  and listing.Name not like '%Tractor%'
  and (ISNULL(CrimeRateIndex,0)<=300)
  and listing.Name not like '%dark%'
  and listing.Name not like '%zero cash flow%'
  and listing.Name not like '%Outlot%'
  and listing.Name not like '%Outparcel%'
  and listing.Name not like '%Out parcel%'
  and listing.Name not like '%Carveout%'
  and (DATEDIFF(year, GETDATE(),listing.CurrentTermExpirationDate) >= 6
  or listing.CurrentTermExpirationDate is null)
  
), cte_TSCOListing as (
SELECT distinct SUBSTRING(Listing.[Name],0,80) as Name
       ,convert(varchar(15),Listing.ContractPrice) as Contract_Price__c
	   ,case when ContractPrice>0
	  then NOI/ContractPrice
	  end as CapRate
      --,[CapRate] as Current_Cap_Rate__c
      ,convert(varchar(15),Listing.noi) as tCurrent_NOI__c
	  ,convert(varchar(15),Listing.noi) as Purchase_NOI__c
      ,convert(varchar(15),Listing.SqFt) as Sq_Ft__c
      --,cast([Years Remaining On Lease] as varchar(15)) as Current_Term_Remaining_years__c --formula field
     -- ,[Lot Size] as Lot_Size_Sq_Ft__c --formula field
      ,SUBSTRING(TRIM(Listing.[YearBuilt]),0,5) as Year_Built__c
      ,Listing.street as Street__c
      ,Listing.[city] as City__c
      ,case when len(Listing.[state]) > 2
	  then null
	  else Listing.[state] 
	  end as State__c
      ,cast(Listing.zip as varchar(50)) as Zip__c
      ,Listing.ListingURL as Listing_URL__c
     -- ,[BrokerWebsiteSource] as Seller_Brokerage__c
      --,[PropertyType] Field Needs to be created
      --,[PropertySubType] Field needs to be created
      ,Listing.TypeOfOwnership as Type_of_Ownership__c
      ,cast(Listing.[YearRenovated] as varchar(15)) as Year_Renovated__c
      --,[LeaseOptions]
      --,[TenantCredit]
      ,Listing.CurrentTermExpirationDate as Current_Term_Expiration_Date__c
	  ,Listing.NetLeaseType as NN_or_NNN__c
	  ,convert(varchar(50),[1MileRadiusPopulation]) as X1_Mile_Radius_Population__c
      ,convert(varchar(50),[MedianHouseholdIncome]) as Median_Household_Income__c
      ,convert(varchar(50),[1MileRadiusVacantHousing]) as X1_Mile_Radius_Vacant_Housing__c
      ,convert(varchar(50),[1MileRadiusOwnerOccupiedHousing]) as X1_Mile_Radius_Owner_Occupied_Housing__c
      ,convert(varchar(50),[1MileRadiusRenterOccupiedHousing]) as X1_Mile_Radius_Renter_Occupied_Housing__c
      ,convert(varchar(50),[1MileRadiusAvgHHIncome]) as X1_Mile_Radius_Avg_HH_Income__c
      ,convert(varchar(50),[3MileRadiusPopulation]) as X3_Mile_Radius_Population__c
      ,convert(varchar(50),[3MileRadiusVacantHousing]) as X3_Mile_Radius_Vacant_Housing__c
      ,convert(varchar(50),[3MileRadiusOwnerOccupiedHousing]) as X3_Mile_Radius_Owner_Occupied_Housing__c
      ,convert(varchar(50),[3MileRadiusRenterOccupiedHousing]) as X3_Mile_Radius_Renter_Occupied_Housing__c
      ,convert(varchar(50),[3MileRadiusAvgHHIncome]) as X3_Mile_Radius_Avg_HH_Income__c
      ,convert(varchar(50),[5MileRadiusPopulation]) as X5_Mile_Radius_Population__c
      ,convert(varchar(50),[5MileRadiusVacantHousing]) as X5_Mile_Radius_Vacant_Housing__c
      ,convert(varchar(50),[5MileRadiusOwnerOccupiedHousing]) as X5_Mile_Radius_Owner_Occupied_Housing__c
      ,convert(varchar(50),[5MileRadiusRenterOccupiedHousing]) as X5_Mile_Radius_Renter_Occupied_Housing__c
	  ,'Listing-Active' as Stage__c
	  ,'a1W0b000007SYyM' as Portfolio__c,
	  vba.id as Seller_Agent__c,
	  vps.id  as Seller_Brokerage__c,
	  case when Listing.ListingCreatedDate = ''
	  then null
	  else convert(varchar(20),CAST(Listing.ListingCreatedDate as smalldatetime),126) 
	   end as Listing_Created_Date__c,
	   Listing.ListingWebsiteId as Listing_Website_Id__c,
	   convert(varchar(50),Listing.CrimeRateIndex) as Crime_Rate_Index__c
  FROM [AcquisitionCandidateAggregator].[dbo].[vBrokerWebsiteDataApprovedTenant] Listing
  left join SalesForce_Reporting.SF_FULLCOPY.vBrokerAgent vba on vba.firstName+' '+vba.lastName = Listing.[BrokerOfRecordName]
  --left join SalesForce_Reporting.dbo.vTenant SF_tenant on SF_tenant.Name = Listing.Tenant
  left join SalesForce_Reporting.SF_FULLCOPY.vPropertyWithListingURL SF_property on SF_Property.Listing_Website_Id__c = Listing.ListingWebsiteId
																			--and SF_Property.Name = SUBSTRING(Listing.[Name],0,80)
  left join SalesForce_Reporting.SF_FULLCOPY.vPropertySource vps on cast(isnull(vps.Name ,'')as varchar(50)) = cast(isnull(Listing.sellerBrokerageName , '')as varchar(50))
  where SF_Property.id is null
  and Listing.Active = 1
  and (ISNULL([5MileRadiusPopulation],50000) >= 30000)
  --and vba.id is not null
  and Listing.Name like '%Tractor%'
  and (ISNULL(CrimeRateIndex,0)<=300)
  and listing.Name not like '%dark%'
  and listing.Name not like '%zero cash flow%'
  and listing.Name not like '%Outlot%'
  and listing.Name not like '%Outparcel%'
  and listing.Name not like '%Out parcel%'
  and listing.Name not like '%Carveout%'
  and (DATEDIFF(year, GETDATE(),listing.CurrentTermExpirationDate) >= 6
  or listing.CurrentTermExpirationDate is null)
), cte_finalPrepped as (


select distinct *
from cte_nonTSCOListing
where ISNULL(CapRate,6)>=0.055
union
select distinct *
from cte_TSCOListing
where ISNULL(CapRate,6)>=0.055
), cte_RowNumber as (
--next, get top 1 record where multiple listing agents are identified.

SELECT distinct Name
       ,Contract_Price__c
      --,[CapRate] as Current_Cap_Rate__c
      ,tCurrent_NOI__c
	  ,Purchase_NOI__c
      ,Sq_Ft__c
      --,cast([Years Remaining On Lease] as varchar(15)) as Current_Term_Remaining_years__c --formula field
     -- ,[Lot Size] as Lot_Size_Sq_Ft__c --formula field
      ,Year_Built__c
      ,Street__c
      ,City__c
      ,State__c
      ,Zip__c
      ,Listing_URL__c
     -- ,[BrokerWebsiteSource] as Seller_Brokerage__c
      --,[PropertyType] Field Needs to be created
      --,[PropertySubType] Field needs to be created
      ,Type_of_Ownership__c
      ,Year_Renovated__c
      --,[LeaseOptions]
      --,[TenantCredit]
      ,convert(varchar(50),Current_Term_Expiration_Date__c) as Current_Term_Expiration_Date__c
	  ,NN_or_NNN__c
	  ,X1_Mile_Radius_Population__c
      ,Median_Household_Income__c
      ,X1_Mile_Radius_Vacant_Housing__c
      ,X1_Mile_Radius_Owner_Occupied_Housing__c
      ,X1_Mile_Radius_Renter_Occupied_Housing__c
      ,X1_Mile_Radius_Avg_HH_Income__c
      ,X3_Mile_Radius_Population__c
      ,X3_Mile_Radius_Vacant_Housing__c
      ,X3_Mile_Radius_Owner_Occupied_Housing__c
      ,X3_Mile_Radius_Renter_Occupied_Housing__c
      ,X3_Mile_Radius_Avg_HH_Income__c
      ,X5_Mile_Radius_Population__c
      ,X5_Mile_Radius_Vacant_Housing__c
      ,X5_Mile_Radius_Owner_Occupied_Housing__c
      ,X5_Mile_Radius_Renter_Occupied_Housing__c
	  ,Stage__c
	  ,Portfolio__c,
	 Seller_Agent__c,
	  Seller_Brokerage__c,
	  convert(varchar(50),Listing_Created_Date__c) as Listing_Created_Date__c,
	   Listing_Website_Id__c,
	   Crime_Rate_Index__c,
	   ROW_NUMBER() over ( Partition By Listing_Website_Id__c Order by Seller_Agent__c desc) as rownum
	   from cte_finalPrepped
	   )
	   select distinct Name
       ,Contract_Price__c
      --,[CapRate] as Current_Cap_Rate__c
      ,tCurrent_NOI__c
	  ,Purchase_NOI__c
      ,Sq_Ft__c
      --,cast([Years Remaining On Lease] as varchar(15)) as Current_Term_Remaining_years__c --formula field
     -- ,[Lot Size] as Lot_Size_Sq_Ft__c --formula field
      ,Year_Built__c
      ,Street__c
      ,City__c
      ,State__c
      ,Zip__c
      ,Listing_URL__c
     -- ,[BrokerWebsiteSource] as Seller_Brokerage__c
      --,[PropertyType] Field Needs to be created
      --,[PropertySubType] Field needs to be created
      ,Type_of_Ownership__c
      ,Year_Renovated__c
      --,[LeaseOptions]
      --,[TenantCredit]
      ,Current_Term_Expiration_Date__c
	  ,NN_or_NNN__c
	  ,X1_Mile_Radius_Population__c
      ,Median_Household_Income__c
      ,X1_Mile_Radius_Vacant_Housing__c
      ,X1_Mile_Radius_Owner_Occupied_Housing__c
      ,X1_Mile_Radius_Renter_Occupied_Housing__c
      ,X1_Mile_Radius_Avg_HH_Income__c
      ,X3_Mile_Radius_Population__c
      ,X3_Mile_Radius_Vacant_Housing__c
      ,X3_Mile_Radius_Owner_Occupied_Housing__c
      ,X3_Mile_Radius_Renter_Occupied_Housing__c
      ,X3_Mile_Radius_Avg_HH_Income__c
      ,X5_Mile_Radius_Population__c
      ,X5_Mile_Radius_Vacant_Housing__c
      ,X5_Mile_Radius_Owner_Occupied_Housing__c
      ,X5_Mile_Radius_Renter_Occupied_Housing__c
	  ,Stage__c
	  ,Portfolio__c,
	 Seller_Agent__c,
	  Seller_Brokerage__c,
	  Listing_Created_Date__c,
	   Listing_Website_Id__c,
	   Crime_Rate_Index__c--,
	   --'Aggregator Listing' as Property_Origin__c
	   from cte_RowNumber 
	   where rownum = 1
GO
