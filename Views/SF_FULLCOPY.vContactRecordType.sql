SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [SF_FULLCOPY].[vContactRecordType] as 
select *
From openquery(SalesforceDevartFULLCOPY, 'select rt.Id, rt.Name  from Contact c
join RecordType rt on rt.id = c.RecordTypeId
where rt.name in (''Dually Registered'', ''RIA'', ''Broker-Dealer'') group by rt.Id, rt.name')
GO
