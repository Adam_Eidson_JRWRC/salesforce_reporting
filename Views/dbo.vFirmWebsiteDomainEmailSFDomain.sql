SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vFirmWebsiteDomainEmailSFDomain] as
with cte_FirmWebsiteDomain As (
SELECT fw.*, f.name,
f.FINRANumber,
REPLACE(REPLACE(REPLACE(fw.website, 'HTTP://',''), 'HTTPS://',''), 'WWW.','') as WebsiteDomain
  FROM [FINRA].[dbo].[Firm_Website] fw
  join FINRA.dbo.Firm f on f.FirmId = fw.FirmId
  ), cte_SFContactEmailDomain as (
  select *,
  SUBSTRING(email, CHARINDEX('@', email)+1,50) as emailDomain
  From openquery(SALESFORCEDEVART, 'Select id,FirstName, LastName, email, Broker_Dealer__c from Contact where CRD__c is null and email is not null')
  ), cte_BrokerDealer as (
   select *
   From openquery(SALESFORCEDEVART, 'Select Id, Name, Firm_CRD__c from Broker_Dealer__c ')
  )

  select distinct csf.*, cfwd.finraNumber,
  cbd.name as Broker_Dealer_Name
  From cte_FirmWebsiteDomain cfwd
  join cte_SFContactEmailDomain csf on csf.emailDomain = cfwd.WebsiteDomain
  join cte_BrokerDealer cbd on cbd.Id = csf.Broker_Dealer__c
  where emailDomain not in (
  'gmail.com')
GO
