SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create VIEW [SF_FULLCOPY].[BrokerAgentSellerBrokerageBestGuess] as 
with cte_AgentSellerBrokerage as (
Select vba.*,
case when sb.name != 'CREXi'
then sb.name
else cab.SellerBrokerage
end as sellerBrokerage
From SalesForce_Reporting.SF_FULLCOPY.vBrokerAgent vba 
join [AcquisitionCandidateAggregator].[dbo].[BrokerAgent] ba on vba.FirstName+' '+vba.LastName = ba.FirstName+' '+ba.LastName
join  [AcquisitionCandidateAggregator].[dbo].ListingBrokerOfRecord lbor on lbor.Name = ba.FirstName+' '+ba.LastName
join [AcquisitionCandidateAggregator].[dbo].listing l on l.id = lbor.ListingId
join [AcquisitionCandidateAggregator].[dbo].SellerBrokerage sb on sb.id = l.SellerBrokerageId
left join AcquisitionCandidateAggregator.STAGE.CREXiAssetBroker cab on cab.ListingId = l.ListingWebsiteId
), cte_SBPrep as (
select id,
sellerBrokerage,
count(*) as SBCount
from cte_AgentSellerBrokerage
group by id,sellerBrokerage
)
select *,
ROW_NUMBER() over(partition by id order by SBCount desc) as rowNumber
from cte_SBPrep
where sellerBrokerage is not null
GO
