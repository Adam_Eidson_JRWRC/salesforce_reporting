SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [SF_FULLCOPY].[vPropertyListingExpired] as 
select vpl.id,
'Listing-Expired' as Stage__c
From SalesForce_Reporting.SF_FULLCOPY.vPropertyWithListingURL vpl
left join AcquisitionCandidateAggregator.dbo.vBrokerWebsiteDataApprovedTenant vbd on vpl.Listing_URL__c = REPLACE(REPLACE(vbd.PropertyURL,CHAR(13), ''), CHAR(10),'')
where vbd.listingName is null
and vpl.Stage__c = 'Listing-Active'
GO
