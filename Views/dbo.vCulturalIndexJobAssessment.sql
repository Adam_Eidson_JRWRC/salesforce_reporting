SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vCulturalIndexJobAssessment] as
Select distinct ja.Id, 
ja.Submission_Id__c,  
ja.Candidate__c,
ja.Employee__c
from dbo.vJob_Assessment ja
join dbo.vCulturalIndexRecordType rt on rt.Id = ja.RecordTypeId 
left join dbo.vJob_Candidate c on c.Id = ja.Candidate__c
left join dbo.vEmployee e on e.Id = ja.Employee__c

GO
